USE [master]
GO
/****** Object:  Database [design]    Script Date: 2020/9/26 13:53:48 ******/
CREATE DATABASE [design]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'design', FILENAME = N'D:\Sql Sever\MSSQL15.MSSQLSERVER\MSSQL\DATA\design.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'design_log', FILENAME = N'D:\Sql Sever\MSSQL15.MSSQLSERVER\MSSQL\DATA\design_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [design] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [design].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [design] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [design] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [design] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [design] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [design] SET ARITHABORT OFF 
GO
ALTER DATABASE [design] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [design] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [design] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [design] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [design] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [design] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [design] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [design] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [design] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [design] SET  DISABLE_BROKER 
GO
ALTER DATABASE [design] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [design] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [design] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [design] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [design] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [design] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [design] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [design] SET RECOVERY FULL 
GO
ALTER DATABASE [design] SET  MULTI_USER 
GO
ALTER DATABASE [design] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [design] SET DB_CHAINING OFF 
GO
ALTER DATABASE [design] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [design] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [design] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'design', N'ON'
GO
ALTER DATABASE [design] SET QUERY_STORE = OFF
GO
USE [design]
GO
/****** Object:  Table [dbo].[Back_Book]    Script Date: 2020/9/26 13:53:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Back_Book](
	[U_no] [char](9) NOT NULL,
	[B_name] [varchar](50) NOT NULL,
	[Return_date] [date] NULL,
	[Actual_days] [int] NULL,
	[Fine] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[U_no] ASC,
	[B_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Book]    Script Date: 2020/9/26 13:53:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book](
	[B_name] [varchar](50) NOT NULL,
	[B_writer] [varchar](20) NOT NULL,
	[B_publish] [varchar](50) NOT NULL,
	[B_price] [float] NOT NULL,
	[Class_no] [char](10) NULL,
	[Num_total] [int] NOT NULL,
	[Num_remain] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[B_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Borrow]    Script Date: 2020/9/26 13:53:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Borrow](
	[U_no] [char](9) NOT NULL,
	[B_name] [varchar](50) NOT NULL,
	[Borrow_date] [date] NOT NULL,
	[Borrow_days] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[U_no] ASC,
	[B_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Classfy]    Script Date: 2020/9/26 13:53:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Classfy](
	[Class_no] [char](10) NOT NULL,
	[B_class] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Class_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 2020/9/26 13:53:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[U_no] [char](9) NOT NULL,
	[U_pad] [char](10) NOT NULL,
	[U_name] [varchar](20) NOT NULL,
	[U_sex] [char](2) NOT NULL,
	[U_unit] [varchar](50) NOT NULL,
	[U_tel] [char](11) NOT NULL,
	[Typer] [char](10) NOT NULL,
 CONSTRAINT [PK__Users__5A381BB45C2B2932] PRIMARY KEY CLUSTERED 
(
	[U_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Back_Book] ([U_no], [B_name], [Return_date], [Actual_days], [Fine]) VALUES (N'182056201', N'C#', CAST(N'2020-09-25' AS Date), 1, 0)
INSERT [dbo].[Back_Book] ([U_no], [B_name], [Return_date], [Actual_days], [Fine]) VALUES (N'182056201', N'Web', CAST(N'2020-09-26' AS Date), 1, 0)
INSERT [dbo].[Back_Book] ([U_no], [B_name], [Return_date], [Actual_days], [Fine]) VALUES (N'182056201', N'计算机组成', CAST(N'2020-09-26' AS Date), 1, 0)
INSERT [dbo].[Book] ([B_name], [B_writer], [B_publish], [B_price], [Class_no], [Num_total], [Num_remain]) VALUES (N'C#', N'wy', N'人民邮电', 13.5, N'1         ', 30, 11)
INSERT [dbo].[Book] ([B_name], [B_writer], [B_publish], [B_price], [Class_no], [Num_total], [Num_remain]) VALUES (N'C++', N'谭浩强', N'清华', 35, N'1         ', 12, 0)
INSERT [dbo].[Book] ([B_name], [B_writer], [B_publish], [B_price], [Class_no], [Num_total], [Num_remain]) VALUES (N'Java', N'wy', N'西安电子', 15.3, N'1         ', 30, 28)
INSERT [dbo].[Book] ([B_name], [B_writer], [B_publish], [B_price], [Class_no], [Num_total], [Num_remain]) VALUES (N'Web', N'wy', N'清华', 12, N'4         ', 21, 11)
INSERT [dbo].[Book] ([B_name], [B_writer], [B_publish], [B_price], [Class_no], [Num_total], [Num_remain]) VALUES (N'高数', N'wy', N'北京', 23, N'2         ', 32, 17)
INSERT [dbo].[Book] ([B_name], [B_writer], [B_publish], [B_price], [Class_no], [Num_total], [Num_remain]) VALUES (N'计算机组成', N'wy', N'清华', 30, N'1         ', 30, 11)
INSERT [dbo].[Borrow] ([U_no], [B_name], [Borrow_date], [Borrow_days]) VALUES (N'182056201', N'Java', CAST(N'2020-09-26' AS Date), 30)
INSERT [dbo].[Classfy] ([Class_no], [B_class]) VALUES (N'1         ', N'计算机')
INSERT [dbo].[Classfy] ([Class_no], [B_class]) VALUES (N'2         ', N'语文')
INSERT [dbo].[Classfy] ([Class_no], [B_class]) VALUES (N'3         ', N'艺术')
INSERT [dbo].[Classfy] ([Class_no], [B_class]) VALUES (N'4         ', N'数学')
INSERT [dbo].[Classfy] ([Class_no], [B_class]) VALUES (N'5         ', N'体育类')
INSERT [dbo].[Users] ([U_no], [U_pad], [U_name], [U_sex], [U_unit], [U_tel], [Typer]) VALUES (N'182056201', N'0000      ', N'速度', N'男', N'山西省撒旦撒', N'12356428256', N'借阅者    ')
INSERT [dbo].[Users] ([U_no], [U_pad], [U_name], [U_sex], [U_unit], [U_tel], [Typer]) VALUES (N'182056204', N'root      ', N'wy', N'女', N'太原工业学院', N'12345682595', N'管理员    ')
INSERT [dbo].[Users] ([U_no], [U_pad], [U_name], [U_sex], [U_unit], [U_tel], [Typer]) VALUES (N'182056224', N'0000      ', N'杨涵', N'女', N'太原工业学院', N'00000000000', N'借阅者    ')
ALTER TABLE [dbo].[Back_Book]  WITH CHECK ADD FOREIGN KEY([B_name])
REFERENCES [dbo].[Book] ([B_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Back_Book]  WITH CHECK ADD  CONSTRAINT [FK__Back_Book__U_no__30F848ED] FOREIGN KEY([U_no])
REFERENCES [dbo].[Users] ([U_no])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Back_Book] CHECK CONSTRAINT [FK__Back_Book__U_no__30F848ED]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD FOREIGN KEY([Class_no])
REFERENCES [dbo].[Classfy] ([Class_no])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Borrow]  WITH CHECK ADD FOREIGN KEY([B_name])
REFERENCES [dbo].[Book] ([B_name])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Borrow]  WITH CHECK ADD  CONSTRAINT [FK__Borrow__U_no__2D27B809] FOREIGN KEY([U_no])
REFERENCES [dbo].[Users] ([U_no])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Borrow] CHECK CONSTRAINT [FK__Borrow__U_no__2D27B809]
GO
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [Str_Num] CHECK  (([Num_remain]>=(0) AND [Num_remain]<=[Num_total]))
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [Str_Num]
GO
ALTER TABLE [dbo].[Borrow]  WITH CHECK ADD  CONSTRAINT [Str_days] CHECK  (([Borrow_days]>(0)))
GO
ALTER TABLE [dbo].[Borrow] CHECK CONSTRAINT [Str_days]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [CK__Users__U_sex__24927208] CHECK  (([U_sex]='女' OR [U_sex]='男'))
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [CK__Users__U_sex__24927208]
GO
USE [master]
GO
ALTER DATABASE [design] SET  READ_WRITE 
GO
