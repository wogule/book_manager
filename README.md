# Book_Manager

#### 介绍
基于Qt使用C++在VS2019中实现图书管理系统

#### [点击跳转视频链接](https://www.bilibili.com/video/BV1CD4y1o7Vo/)
#### 结果图片展示

![登录](https://images.gitee.com/uploads/images/2020/0926/123747_48ab9f10_7510148.png "屏幕截图.png")
![注册](https://images.gitee.com/uploads/images/2020/0926/124011_55dcdf99_7510148.png "屏幕截图.png")
![菜单](https://images.gitee.com/uploads/images/2020/0926/123827_87b27b79_7510148.png "屏幕截图.png")
![类别](https://images.gitee.com/uploads/images/2020/0926/123845_fff4f668_7510148.png "屏幕截图.png")
![罚金](https://images.gitee.com/uploads/images/2020/0926/123900_fa13c1e4_7510148.png "屏幕截图.png")
![图书](https://images.gitee.com/uploads/images/2020/0926/123943_ffc49302_7510148.png "屏幕截图.png")