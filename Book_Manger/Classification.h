#pragma once

#include <QDialog>
#include "ui_Classification.h"
#include "DBHelper.h"
class Classification : public QDialog
{
	Q_OBJECT

public:
	Classification(QWidget *parent = Q_NULLPTR);
	~Classification();
	bool SqlQuery();
public slots:
	bool Check();
	bool Add();
	bool Del();
signals:
	void BackClassfy();
private:
	Ui::Classification ui;
};
