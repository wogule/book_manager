#include "Book_Manger.h"
/*DBHelper::OpenDatabase();
   DBHelper::CloseDatabase();
   DBHelper::QueryDatabase();
   DBHelper::ADDData();
   DBHelper::DeleteData();
   DBHelper::UpdateData();*/
QString Book_Manger::U_no = "";
Book_Manger::Book_Manger(QWidget *parent)
    : QDialog(parent)
{
    ui.setupUi(this);
    setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
    connect(ui.Btn_NO, &QPushButton::clicked, this, &QWidget::close);
    connect(ui.Btn_OK, &QPushButton::clicked, this, &Book_Manger::Show_Meau);
    connect(ui.LBtn_R,&QPushButton::clicked,this,&Book_Manger::Show_R);
    connect(&R, &Register::Back_Main, this, &Book_Manger::Show_M);
    connect(&M, &Manager_Meau::Back_Main, this, &Book_Manger::Show_A);
    connect(&RM, &Reader_Meau::Back_Main, this, &Book_Manger::Show_Y);
    
}
void Book_Manger::Show_A() {
    M.hide();
    this->show();
}
void Book_Manger::Show_Y()
{
    RM.hide();
    this->show();
}
void Book_Manger::Show_M() {
    R.hide();//w子窗口隐藏
    this->show(); //主窗口显示
}
void Book_Manger::Show_R() {
    this->hide();
    R.show();
}
bool Book_Manger::Check()
{
    /*
    *1.先得到文本框值
    *2.将其融合为一条sql语句
    *3.调用查询方法进行查询
    *4.if条件进行判断 
    */
    /*这是可以中文不乱码*/
   
    QString SqlContent = QString("SELECT * FROM Users WHERE U_no='%1'and U_pad='%2'and Typer='%3'").arg(ui.U_ledit->text()).arg(ui.P_ledit->text()).arg(ui.RBtn_A->isChecked() ? ui.RBtn_A->text() : ui.RBtn_U->text());
    QByteArray byte;  byte = SqlContent.toLocal8Bit();
    char* ch;  ch = byte.data();
    if (ui.U_ledit->text().isEmpty() || ui.P_ledit->text().isEmpty())
        return false;
    else if (DBHelper::QueryDatabase(ch))
    {
        U_no = ui.U_ledit->text();
        return true;
    }
        
       
    return false;
  
   
}
void Book_Manger::Show_Meau()
{
    /*槽函数
    * 1.调用Check函数判断
    * 2.返回true则跳转
    * 3.返回false则提示错误
    */
    if (Check()&& ui.RBtn_A->isChecked()) {
        QMessageBox::information(this, QString::fromLocal8Bit("登陆成功"), QStringLiteral("<font color='white'>欢迎访问！！！</font>"));
        this->hide();
        M.show();
    }
    else if(Check() && ui.RBtn_U->isChecked()){
        QMessageBox::information(this, QString::fromLocal8Bit("登陆成功"), QStringLiteral("<font color='white'>欢迎访问！！！</font>"));
        this->hide();
        RM.show();
       
    }
    else {
        QMessageBox::critical(this, QString::fromLocal8Bit("登陆失败"), QStringLiteral("<font color='white'>请重新输入完整！！！</font>"));
    }
}