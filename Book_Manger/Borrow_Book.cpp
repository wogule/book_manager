#include "Borrow_Book.h"
#include "Book_Manger.h"
#include <QDateTime>
#include "Book_Manger.h"
Borrow_Book::Borrow_Book(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	connect(ui.Back_Btn, &QPushButton::clicked, [=]() {emit Back_meau(); });
    
    connect(ui.Book_Led, &QLineEdit::textChanged, [=]() {Check(); });
    connect(ui.All_Btn, &QPushButton::clicked, [=]() {SqlQuery(); });
    connect(ui.Book_View, &QTableView::clicked, [=]() {
        int row = ui.Book_View->currentIndex().row();
        QAbstractItemModel* model = ui.Book_View->model();
        QModelIndex index = model->index(row, 0);//选中行第一列的内容
        QString Book_Name = index.data().toString();
        ui.Bname_Led->setText(Book_Name);
        });
    connect(ui.Bor_Btn, &QPushButton::clicked, [=]() {Judge(); });
}

Borrow_Book::~Borrow_Book()
{
}

bool Borrow_Book::SqlQuery(char* SqlContent)
{
    if (DBHelper::OpenDatabase()) {

        QSqlQueryModel* model = new QSqlQueryModel(this);
        model->setQuery(QString::fromLocal8Bit(SqlContent));
        model->setHeaderData(0, Qt::Horizontal, QStringLiteral("书名"));
        model->setHeaderData(1, Qt::Horizontal, QStringLiteral("作者"));
        model->setHeaderData(2, Qt::Horizontal, QStringLiteral("出版"));
        model->setHeaderData(3, Qt::Horizontal, QStringLiteral("价格"));
        model->setHeaderData(4, Qt::Horizontal, QStringLiteral("分类"));
        model->setHeaderData(5, Qt::Horizontal, QStringLiteral("总数"));
        model->setHeaderData(6, Qt::Horizontal, QStringLiteral("剩余"));
        
        ui.Book_View->setModel(model);
        ui.Book_View->setSelectionMode(QAbstractItemView::SingleSelection);//不能连续选
        ui.Book_View->setSelectionBehavior(QAbstractItemView::SelectRows);//按行选
        ui.Book_View->setEditTriggers(QAbstractItemView::NoEditTriggers);//不能在 表格上编辑
        ui.Book_View->horizontalHeader()->setStyleSheet("QHeaderView::section {"
            "background-color:rgb(255, 254, 240,70);}");

        ui.Book_View->verticalHeader()->hide();
        QHeaderView* header = ui.Book_View->horizontalHeader();
        header->setStretchLastSection(false);//拉伸最后一段


        ui.Book_View->setFixedWidth(ui.Book_View->columnWidth(0) +
            ui.Book_View->columnWidth(1) +
            ui.Book_View->columnWidth(2) +
            ui.Book_View->columnWidth(3) +
            ui.Book_View->columnWidth(4) +
            ui.Book_View->columnWidth(5) +
            ui.Book_View->columnWidth(6) + 5);
        return true;
    }
    return false;
}

bool Borrow_Book::Check()
{
    QString SqlContent;
    if (ui.Class_Comb->currentIndex() == 0)//选择按书名查找
        SqlContent = QString("SELECT B_name,B_writer,B_publish,B_price,B_Class,Num_total,Num_remain FROM Book,Classfy WHERE Book.Class_no=Classfy.Class_no AND B_name like'%%1%'").arg(ui.Book_Led->text());
    else//选择按类别查找
        SqlContent = QString("SELECT B_name,B_writer,B_publish,B_price,B_Class,Num_total,Num_remain FROM Book,Classfy WHERE Book.Class_no=Classfy.Class_no AND B_Class like'%%1%'").arg(ui.Book_Led->text());
    QByteArray byte;  byte = SqlContent.toLocal8Bit();
    char* ch;  ch = byte.data();
    if (SqlQuery(ch))
        return true;
    return false;
}

bool Borrow_Book::PreJudge()
{
    QString SqlContent = QString("SELECT U_no,B_name FROM Back_Book WHERE U_no='%1'and B_name='%2'").arg(Book_Manger::U_no).arg(ui.Bname_Led->text());
    QByteArray byte;  byte = SqlContent.toLocal8Bit();
    char* ch;  ch = byte.data();
    if (DBHelper::QueryDatabase(ch))
        return false;
    return true;
}

bool Borrow_Book::Add_Bor()
{
    //判断此书是否以前借过

    QDateTime current_date_time = QDateTime::currentDateTime();
    QString SqlContent = QString("INSERT INTO Borrow VALUES('%1', '%2', '%3', %4)").arg(Book_Manger::U_no).arg(ui.Bname_Led->text()).arg(current_date_time.toString("yyyy.MM.dd")).arg(ui.Date_Led->text());
    QByteArray byte;  byte = SqlContent.toLocal8Bit();
    char* ch;  ch = byte.data();

    if (ui.Bname_Led->text().isEmpty() || ui.Date_Led->text().isEmpty())
        return false;
    else if (PreJudge())
    {
        DBHelper::ADDData(ch);
        return true;
    }
        
    return false;
}

void Borrow_Book::Judge()
{
    if (Add_Bor())
    {
        QString SqlContent = QString("UPDATE Book set Num_remain=Num_remain-1 Where B_name='%1'").arg(ui.Bname_Led->text());
        QByteArray byte;  byte = SqlContent.toLocal8Bit();
        char* ch;  ch = byte.data();
        if (DBHelper::UpdateData(ch)) {
            SqlQuery();
            QMessageBox::information(this, QString::fromLocal8Bit("借书成功"), QStringLiteral("<font color='red'>欢迎再来！！！</font>"));
        }
    } 
    else
    {
        SqlQuery();
        QMessageBox::information(this, QString::fromLocal8Bit("借书失败"), QStringLiteral("<font color='red'>请检查信息或者以前借过！！！</font>"));
    }
        
}