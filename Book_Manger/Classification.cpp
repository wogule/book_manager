#include "Classification.h"

Classification::Classification(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
    setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
    connect(ui.Back_Btn, &QPushButton::clicked, [=]() {emit BackClassfy(); });
    connect(ui.C_View, &QTableView::clicked, [=]() {
        int row = ui.C_View->currentIndex().row();
        QAbstractItemModel* model = ui.C_View->model();
        QModelIndex index = model->index(row, 0);//选中行第一列的内容
        QModelIndex second = model->index(row, 1);
        QString Class_num = index.data().toString();
        QString Class_name = second.data().toString();
        ui.Num_Led->setText(Class_num);
        ui.Class_Led->setText(Class_name);
        });
    connect(ui.Update_Btn, &QPushButton::clicked, this, &Classification::Check);
    connect(ui.Add_Btn, &QPushButton::clicked, this, &Classification::Add);
    connect(ui.Delete_Btn, &QPushButton::clicked, this, &Classification::Del);
}

Classification::~Classification()
{
}

bool Classification::SqlQuery()
{
    if (DBHelper::OpenDatabase()) {

        QSqlQueryModel* model = new QSqlQueryModel(this);
        model->setQuery(QString::fromLocal8Bit("SELECT * from Classfy;"));
        /*QSqlTableModel* model = new QSqlTableModel(this,DBHelper::db);
        model->setTable("Classfy");*/
        model->setHeaderData(0, Qt::Horizontal, QStringLiteral("序号"));
        model->setHeaderData(1, Qt::Horizontal, QStringLiteral("类名"));
        /*model->select();*/
        ui.C_View->setModel(NULL);
        ui.C_View->setModel(model);
       
        ui.C_View->setSelectionMode(QAbstractItemView::SingleSelection);//不能连续选
        ui.C_View->setSelectionBehavior(QAbstractItemView::SelectRows);//按行选
        ui.C_View->setEditTriggers(QAbstractItemView::NoEditTriggers);//不能在 表格上编辑
        ui.C_View->horizontalHeader()->setStyleSheet("QHeaderView::section {"
            "background-color:rgb(255, 254, 240,70);}");
        ui.C_View->verticalHeader()->hide();
        QHeaderView* header = ui.C_View->horizontalHeader();
        header->setStretchLastSection(false);//拉伸最后一段
        ui.C_View->setFixedWidth(ui.C_View->columnWidth(0) +
            ui.C_View->columnWidth(1) + 5);
        
        return true;
    }
    return false;
}
bool Classification::Check()
{
    /*
    *1.先得到文本框值
    *2.将其融合为一条sql语句
    *3.调用查询方法进行查询
    *4.if条件进行判断
    */
    /*这是可以中文不乱码*/
    QString SqlContent = QString("UPDATE  Classfy set B_class='%1'where Class_no='%2'").arg(ui.Class_Led->text()).arg(ui.Num_Led->text());
    QByteArray byte;  byte = SqlContent.toLocal8Bit();
    char* ch;  ch = byte.data();
    if (ui.Class_Led->text().isEmpty() )
        return false;
    else if (DBHelper::UpdateData(ch))
    {
        ui.Num_Led->setText("");
        ui.Class_Led->setText("");
        SqlQuery();
        return true;
    }
        
    return false;


}
bool Classification::Add() {
    QString SqlContent = QString("INSERT INTO  Classfy VALUES( '%1', '%2')").arg(ui.Num_Led->text()).arg(ui.Class_Led->text());
    QByteArray byte;  byte = SqlContent.toLocal8Bit();
    char* ch;  ch = byte.data();
   
    if (ui.Class_Led->text().isEmpty())
        return false;
    else if (DBHelper::ADDData(ch))
    {
        ui.Num_Led->setText("");
        ui.Class_Led->setText("");
        SqlQuery();
        return true;
    }
        
    return false;
}

bool Classification::Del() {
    QString SqlContent = QString("Delete from Classfy WHERE  Class_no='%1'").arg(ui.Num_Led->text());
    QByteArray byte;  byte = SqlContent.toLocal8Bit();
    char* ch;  ch = byte.data();
    if (ui.Class_Led->text().isEmpty())
        return false;
    else if (DBHelper::DeleteData(ch))
    {
        ui.Num_Led->setText("");
        ui.Class_Led->setText("");
        SqlQuery();
        return true;
    }

    return false;
}