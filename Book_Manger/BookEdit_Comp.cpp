#include "BookEdit_Comp.h"
BookEdit_Comp::BookEdit_Comp(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	
}

BookEdit_Comp::~BookEdit_Comp()
{
}
bool BookEdit_Comp::Book_Show(QString Content)//�޸�
{
	Class_Add();
	QString SqlContent = QString("SELECT B_name, B_writer, B_publish, B_price, B_Class, Num_total, Num_remain FROM Book, Classfy WHERE Book.Class_no = Classfy.Class_no and B_name='%1'").arg(Content);
    QByteArray byte;  byte = SqlContent.toLocal8Bit();
    char* ch;  ch = byte.data();
    DBHelper::OpenDatabase();
    QSqlQuery query(DBHelper::db); 
    if (!query.exec(QString::fromLocal8Bit(ch)))
    {
        QMessageBox::critical(0, QObject::tr("Database error"), query.lastError().text());
        DBHelper::CloseDatabase();
        return false;

    }
    while (query.next())
    {
        ui.Name_Led->setText(query.value(0).toString());
        ui.W_Led->setText(query.value(1).toString());
        ui.P_Led->setText(query.value(2).toString());
        ui.M_Led->setText(query.value(3).toString());
		ui.C_Comb->setCurrentText(query.value(4).toString());
        ui.T_Led->setText(query.value(5).toString());
        ui.R_Led->setText(query.value(6).toString());
    }
	if (ui.Name_Led->isReadOnly() == false)
		ui.Name_Led->setReadOnly(true);
    return true;
  
}
bool BookEdit_Comp::Book_Update() {
	int Class_no = ui.C_Comb->currentIndex() + 1;
	QString SqlContent = QString("Update Book set B_writer='%1',B_publish='%2',B_price=%3,Class_no='%4',Num_total=%5,Num_remain=%6 Where B_name='%7'").arg(ui.W_Led->text()).arg(ui.P_Led->text()).arg(ui.M_Led->text()).arg(Class_no).arg(ui.T_Led->text()).arg(ui.R_Led->text()).arg(ui.Name_Led->text());
	QByteArray byte;  byte = SqlContent.toLocal8Bit();
	char* ch;  ch = byte.data();
	
	if (DBHelper::UpdateData(ch))
		return true;
	return false;
}
bool BookEdit_Comp::Book_Add()//������
{
	int Class_no = ui.C_Comb->currentIndex()+1;
	QString SqlContent = QString("INSERT INTO Book VALUES('%1','%2','%3',%4,'%5',%6,%7)").arg(ui.Name_Led->text()).arg(ui.W_Led->text()).arg(ui.P_Led->text()).arg(ui.M_Led->text()).arg(Class_no).arg(ui.T_Led->text()).arg(ui.R_Led->text());
	QByteArray byte;  byte = SqlContent.toLocal8Bit();
	char* ch;  ch = byte.data();
	if (DBHelper::ADDData(ch))
	{
		qDebug() << "add successs";
		
		return true;
	}
		
	return false;
}
void BookEdit_Comp::Class_Add()
{
	QStringList ComBoxContent;
	DBHelper::OpenDatabase();
	QSqlQuery query(DBHelper::db); 
	if (!query.exec(QString::fromLocal8Bit("SELECT  B_Class FROM Classfy ")))
	{
	    QMessageBox::critical(0, QObject::tr("Database error"), query.lastError().text());
	    DBHelper::CloseDatabase();
	    return;
	
	}
	while (query.next())
	{
		QString classes = query.value(0).toString();
		ComBoxContent.append(classes);
	}
	ui.C_Comb->clear();
	ui.C_Comb->addItems(ComBoxContent);
	if (ui.Name_Led->isReadOnly() == true)
		ui.Name_Led->setReadOnly(false);
	ui.Name_Led->setText("");
	ui.W_Led->setText("");
	ui.P_Led->setText("");
	ui.M_Led->setText("");
	ui.T_Led->setText("");
	ui.R_Led->setText("");
}
