#pragma once

#include <QDialog>
#include "ui_Borrows.h"
#include "DBHelper.h"
class Borrows : public QDialog
{
	Q_OBJECT

public:
	Borrows(QWidget *parent = Q_NULLPTR);
	~Borrows();
	bool SqlQuery();
public slots:
	void ReturnMain();
signals:
	void BackBorrow();
private:
	Ui::Borrows ui;
};
