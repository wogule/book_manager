#include "Reader_Meau.h"

Reader_Meau::Reader_Meau(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
	connect(ui.Back_Btn, &QPushButton::clicked, this, &Reader_Meau::BackMain);
	//跳转到借书页面
	connect(ui.Bor_Btn, &QPushButton::clicked, [=]() {
		this->hide();
		Borbook.SqlQuery();
		Borbook.show();
		});
	//还书页面
	connect(ui.BackBook_Btn, &QPushButton::clicked, [=]() {this->hide(); Bacbook.SqlQuery(); Bacbook.show(); });
	
	/////////////////////
	//返回页面
	connect(&Borbook, &Borrow_Book::Back_meau, [=]() {Borbook.hide(); this->show(); });
	connect(&Bacbook, &Back_Book::Back_meau, [=]() {Bacbook.hide(); this->show(); });
	
}

Reader_Meau::~Reader_Meau()
{
}
void Reader_Meau::BackMain() {
	emit Back_Main();
		
}