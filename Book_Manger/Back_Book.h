#pragma once

#include <QDialog>
#include "ui_Back_Book.h"
#include "DBHelper.h"
class Back_Book : public QDialog
{
	Q_OBJECT

public:
	Back_Book(QWidget *parent = Q_NULLPTR);
	~Back_Book();
	bool SqlQuery();
	bool Bacbook();
	bool Update_Bor();
signals:
	void Back_meau();
private:
	Ui::Back_Book ui;
	uint stime;//将借书日期转换成时间戳
	int days;//获取借书时间
};
