#include "User.h"

User::User(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
    setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
    connect(ui.Back_Btn, &QPushButton::clicked, this, &User::ReturnMain);
}

User::~User()
{
}

bool User::SqlQuery()
{
    if (DBHelper::OpenDatabase()) {

        QSqlQueryModel* model = new QSqlQueryModel(this);
        model->setQuery(QString::fromLocal8Bit("SELECT U_no,U_name,U_sex,U_unit,U_tel FROM Users Where Typer='借阅者'"));
        model->setHeaderData(0, Qt::Horizontal, QStringLiteral("学号"));
        model->setHeaderData(1, Qt::Horizontal, QStringLiteral("姓名"));
        model->setHeaderData(2, Qt::Horizontal, QStringLiteral("性别"));
        model->setHeaderData(3, Qt::Horizontal, QStringLiteral("单位"));
        model->setHeaderData(4, Qt::Horizontal, QStringLiteral("电话"));
        
        ui.User_View->setModel(model);
        ui.User_View->setSelectionMode(QAbstractItemView::SingleSelection);//不能连续选
        ui.User_View->setSelectionBehavior(QAbstractItemView::SelectRows);//按行选
        ui.User_View->setEditTriggers(QAbstractItemView::NoEditTriggers);//不能在 表格上编辑
        ui.User_View->horizontalHeader()->setStyleSheet("QHeaderView::section {"
            "background-color:rgb(255, 254, 240,70);}");

        ui.User_View->verticalHeader()->hide();
        QHeaderView* header = ui.User_View->horizontalHeader();
        header->setStretchLastSection(false);//拉伸最后一段


        ui.User_View->setFixedWidth(ui.User_View->columnWidth(0) +
            ui.User_View->columnWidth(1) +
            ui.User_View->columnWidth(2) +
            ui.User_View->columnWidth(3) +
            ui.User_View->columnWidth(4) + 5);
        ui.Back_Btn->setFixedWidth(ui.User_View->width());
        this->setFixedWidth(ui.User_View->width() + ui.User_View->columnWidth(0) / 3);
        return true;
    }
    return false;
}

void User::ReturnMain() {
	emit BackUsers();
}
