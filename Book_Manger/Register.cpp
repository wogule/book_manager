#include "Register.h"


Register::Register(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
	connect(ui.Back_Btn, &QPushButton::clicked, this, &Register::ReturnMain);
	connect(ui.Commit_Btn, &QPushButton::clicked, this, &Register::SureMain);
}

Register::~Register()
{
}
void Register::ReturnMain() {
	emit Back_Main();//执行信号
}
bool Register::CheckAddData()
{
	/*
	*1.先得到文本框值
	*2.将其融合为一条sql语句
	*3.调用添加方法
	*4.if条件进行判断
	*/
	QString SqlContent = QString("INSERT INTO Users VALUES('%1', '%2', '%3', '%4', '%5', '%6', '%7')").arg(ui.U_Led->text()).arg(ui.P_Led->text()).arg(ui.N_Led->text()).arg(ui.M_RBtn->isChecked()?ui.M_RBtn->text():ui.F_RBtn->text()).arg(ui.Unit_Led->text()).arg(ui.Num_Led->text()).arg(ui.RBtn_NO->isChecked()? QStringLiteral("借阅者"): QStringLiteral("管理员"));
	QByteArray byte;  byte = SqlContent.toLocal8Bit();
	char* ch;  ch = byte.data();
	if (ui.U_Led->text().isEmpty()|| ui.P_Led->text().isEmpty()|| ui.N_Led->text().isEmpty()|| ui.Unit_Led->text().isEmpty()|| ui.Num_Led->text().isEmpty())
		return false;
	else if (DBHelper::ADDData(ch)) 
			return true;
	return false;
}
void Register::SureMain()
{
	if (CheckAddData())
	{
		QMessageBox::information(0, QString::fromLocal8Bit("注册成功"), QStringLiteral("欢迎访问！！！"));
		
		emit Back_Main();
	}
	else
	{
		QMessageBox::critical(0, QString::fromLocal8Bit("注册失败"), QStringLiteral("请重新填写完整！！！"));
	}
}