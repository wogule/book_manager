#include "Borrows.h"

Borrows::Borrows(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
    setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);

    connect(ui.Back_Btn, &QPushButton::clicked, this, &Borrows::ReturnMain);
    
}

Borrows::~Borrows()
{
}
void Borrows::ReturnMain() {
	emit BackBorrow();
}

bool Borrows::SqlQuery()
{
    if (DBHelper::OpenDatabase()) {

        QSqlQueryModel* model = new QSqlQueryModel(this);
        model->setQuery(QString::fromLocal8Bit("SELECT * FROM Borrow"));
        model->setHeaderData(0, Qt::Horizontal, QStringLiteral("学号"));
        model->setHeaderData(1, Qt::Horizontal, QStringLiteral("书名"));
        model->setHeaderData(2, Qt::Horizontal, QStringLiteral("日期"));
        model->setHeaderData(3, Qt::Horizontal, QStringLiteral("天数"));
        ui.Bor_View->setModel(model);
        ui.Bor_View->setSelectionMode(QAbstractItemView::SingleSelection);//不能连续选
        ui.Bor_View->setSelectionBehavior(QAbstractItemView::SelectRows);//按行选
        ui.Bor_View->setEditTriggers(QAbstractItemView::NoEditTriggers);//不能在 表格上编辑
        ui.Bor_View->horizontalHeader()->setStyleSheet("QHeaderView::section {"
            "background-color:rgb(255, 254, 240,70);}");

        ui.Bor_View->verticalHeader()->hide();
        QHeaderView* header = ui.Bor_View->horizontalHeader();
        header->setStretchLastSection(false);//拉伸最后一段


        ui.Bor_View->setFixedWidth(ui.Bor_View->columnWidth(0) +
            ui.Bor_View->columnWidth(1) +
            ui.Bor_View->columnWidth(2) +
            ui.Bor_View->columnWidth(3) +5);
        ui.Back_Btn->setFixedWidth(ui.Bor_View->width());
        this->setFixedWidth(ui.Bor_View->width()+ ui.Bor_View->columnWidth(0)/3);
        return true;
    }
    return false;
}