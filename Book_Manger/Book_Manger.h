#pragma once

#include <QtWidgets/QDialog>
#include "ui_Book_Manger.h"
#include "Register.h"
#include "Manager_Meau.h"
#include "DBHelper.h"
#include "Reader_Meau.h"

class Book_Manger : public QDialog
{
    Q_OBJECT

public:
    Book_Manger(QWidget *parent = Q_NULLPTR);
    bool Check();//判断用户输入的账号密码是否正确
public slots:
    void Show_M();//从注册界面返回到登录界面
    void Show_R();//从登录界面到注册界面
    void Show_A();//从菜单界面返回登录界面
    void Show_Y();//从菜单返回登录
    void Show_Meau();//从登录界面到注册界面转到下一个界面
signals:
    
private:
    Ui::Book_MangerClass ui;
    Register R;
    Manager_Meau M;
    Reader_Meau RM;
public:
    static QString U_no;
};
