#pragma once

#include <QDialog>
#include "ui_Borrow_Book.h"
#include "DBHelper.h"

class Borrow_Book : public QDialog
{
	Q_OBJECT

public:
	Borrow_Book(QWidget *parent = Q_NULLPTR);
	~Borrow_Book();
	bool SqlQuery(char* SqlContent= const_cast < char*>("SELECT B_name,B_writer,B_publish,B_price,B_Class,Num_total,Num_remain FROM Book,Classfy WHERE Book.Class_no=Classfy.Class_no"));
	bool Check();
	bool PreJudge();
	bool Add_Bor();
	void Judge();
signals:
	void Back_meau();
private:
	Ui::Borrow_Book ui;
};
