#pragma once

#include <QDialog>
#include "ui_User.h"
#include "DBHelper.h"
class User : public QDialog
{
	Q_OBJECT

public:
	User(QWidget *parent = Q_NULLPTR);
	~User();
	bool SqlQuery();
public slots:
	void ReturnMain();
signals:
	void BackUsers();
private:
	Ui::User ui;
};
