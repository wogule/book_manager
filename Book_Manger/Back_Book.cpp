#include "Back_Book.h"
#include "Book_Manger.h"
#include <QDateTime>
Back_Book::Back_Book(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
    setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
    connect(ui.Back_Btn, &QPushButton::clicked, [=]() {emit Back_meau(); });
    connect(ui.Back_View, &QTableView::clicked, [=]() {
        int row = ui.Back_View->currentIndex().row();
        QAbstractItemModel* model = ui.Back_View->model();
        QModelIndex index = model->index(row, 1);//选中行第二列的内容
        QModelIndex time = model->index(row, 2);
        QString first= time.data().toString();
        days = model->index(row, 3).data().toInt();
        QDateTime start = QDateTime::fromString(first, "yyyy-MM-dd");
        stime= start.toTime_t();
        QString Book_Name = index.data().toString();
        ui.BackBook_Led->setText(Book_Name);
        });
    connect(ui.BackBook_Btn, &QPushButton::clicked, [=]() {Update_Bor(); });
}

Back_Book::~Back_Book()
{
}
// 
bool Back_Book::SqlQuery()
{
    QString SqlContent = QString("SELECT U_no, B_name, Borrow_date, Borrow_days FROM Borrow WHere U_no = '%1'").arg(Book_Manger::U_no);
    QByteArray byte;  byte = SqlContent.toLocal8Bit();
    char* ch;  ch = byte.data();
    if (DBHelper::OpenDatabase()) {

        QSqlQueryModel* model = new QSqlQueryModel(this);
        model->setQuery(QString::fromLocal8Bit(ch));
        model->setHeaderData(0, Qt::Horizontal, QStringLiteral("学号"));
        model->setHeaderData(1, Qt::Horizontal, QStringLiteral("书名"));
        model->setHeaderData(2, Qt::Horizontal, QStringLiteral("时间"));
        model->setHeaderData(3, Qt::Horizontal, QStringLiteral("天数"));
        

        ui.Back_View->setModel(model);
        ui.Back_View->setSelectionMode(QAbstractItemView::SingleSelection);//不能连续选
        ui.Back_View->setSelectionBehavior(QAbstractItemView::SelectRows);//按行选
        ui.Back_View->setEditTriggers(QAbstractItemView::NoEditTriggers);//不能在 表格上编辑
        ui.Back_View->horizontalHeader()->setStyleSheet("QHeaderView::section {"
            "background-color:rgb(255, 254, 240,70);}");

        ui.Back_View->verticalHeader()->hide();
        QHeaderView* header = ui.Back_View->horizontalHeader();
        header->setStretchLastSection(false);//拉伸最后一段


        ui.Back_View->setFixedWidth(ui.Back_View->columnWidth(0) +
            ui.Back_View->columnWidth(1) +
            ui.Back_View->columnWidth(2) +
            ui.Back_View->columnWidth(3) + 5);
        return true;
    }
    return false;
}

bool Back_Book::Bacbook()
{
    float Fines=0,StandardMoney = 1.5;
    QDateTime ends, current_date_time = QDateTime::currentDateTime();
    QString end = current_date_time.toString("yyyy.MM.dd");
    ends = QDateTime::fromString(end, "yyyy.MM.dd");
    uint etime = ends.toTime_t();
    int ndaysec = 24 * 60 * 60;
    int Days= (etime - stime) / (ndaysec)+((etime - stime) % (ndaysec)+(ndaysec - 1)) / (ndaysec);
    if (Days >= days)
        Fines = (Days - days) * StandardMoney;
    QString SqlContent = QString("INSERT INTO Back_Book VALUES('%1','%2','%3',%4,%5)").arg(Book_Manger::U_no).arg(ui.BackBook_Led->text()).arg(current_date_time.toString("yyyy.MM.dd")).arg(Days).arg(Fines);
    QByteArray byte;  byte = SqlContent.toLocal8Bit();
    char* ch;  ch = byte.data();
    if (DBHelper::ADDData(ch))
        return true;
    return false;
}
bool Back_Book::Update_Bor()
{
    if (Bacbook())
    {
        QString SqlContent = QString("DELETE FROM Borrow WHERE U_no='%1'AND B_name='%2'").arg(Book_Manger::U_no).arg(ui.BackBook_Led->text());
        QByteArray byte;  byte = SqlContent.toLocal8Bit();
        char* ch;  ch = byte.data();
        if (DBHelper::DeleteData(ch))
        {
            SqlQuery();
            return true;
        }
            
    }
    else
    {
        SqlQuery();
        QMessageBox::information(this, QString::fromLocal8Bit("还书失败"), QStringLiteral("<font color='red'>请重新操作！！！</font>"));
    }
    return false;
}