#include "Forfeit.h"

Forfeit::Forfeit(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
    setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
    connect(ui.Back_Btn, &QPushButton::clicked, this, &Forfeit::Return_Main);
}

Forfeit::~Forfeit()
{
}
bool Forfeit::SqlQuery()
{
    if (DBHelper::OpenDatabase()) {

        QSqlQueryModel* model = new QSqlQueryModel(this);
        model->setQuery(QString::fromLocal8Bit("SELECT * FROM Back_Book"));
        model->setHeaderData(0, Qt::Horizontal, QStringLiteral("借阅学号"));
        model->setHeaderData(1, Qt::Horizontal, QStringLiteral("借阅书名"));
        model->setHeaderData(2, Qt::Horizontal, QStringLiteral("返回日期"));
        model->setHeaderData(3, Qt::Horizontal, QStringLiteral("实际天数"));
        model->setHeaderData(4, Qt::Horizontal, QStringLiteral("需还钱数"));
        ui.Fine_View->setModel(model);
        ui.Fine_View->setSelectionMode(QAbstractItemView::SingleSelection);//不能连续选
        ui.Fine_View->setSelectionBehavior(QAbstractItemView::SelectRows);//按行选
        ui.Fine_View->setEditTriggers(QAbstractItemView::NoEditTriggers);//不能在 表格上编辑
        ui.Fine_View->horizontalHeader()->setStyleSheet("QHeaderView::section {"
            "background-color:rgb(255, 254, 240,70);}");

        ui.Fine_View->verticalHeader()->hide();
        QHeaderView* header = ui.Fine_View->horizontalHeader();
        header->setStretchLastSection(false);//拉伸最后一段


        ui.Fine_View->setFixedWidth(ui.Fine_View->columnWidth(0) +
            ui.Fine_View->columnWidth(1) +
            ui.Fine_View->columnWidth(2) +
            ui.Fine_View->columnWidth(3) +
            ui.Fine_View->columnWidth(4) + 5);
        ui.Back_Btn->setFixedWidth(ui.Fine_View->width());
        this->setFixedWidth(ui.Fine_View->width() + ui.Fine_View->columnWidth(0) / 3);
        return true;
    }
    return false;
}
void Forfeit::Return_Main() {
    emit BackFine();
}