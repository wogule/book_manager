#pragma once
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlTableModel>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QDebug>
#include <QMessageBox>
#include<string>
using namespace std;
class DBHelper
{
public:
	static QSqlDatabase  db;
public:
	
	 static bool OpenDatabase();//打开数据库
	 static bool CloseDatabase();//关闭数据库
	 //查询= "SELECT * FORM Users"
	 static bool QueryDatabase(char* SqlContent);
	 //添加= "INSERT INTO Users VALUES('182056202', '0001', '我m', '男', '山西省', '12345685121', '借阅者')"
	 static bool ADDData(char* SqlContent);
	 //删除= "DELETE FROM Users WHERE U_no='182056201'"
	 static bool DeleteData(char* SqlContent);
	 //修改 = "UPDATE  Users set U_sex='女' WHERE U_no='182056204'"
	 static bool UpdateData(char* SqlContent);
};
 
