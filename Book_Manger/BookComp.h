#pragma once

#include <QWidget>
#include "ui_BookComp.h"
#include "Book_Edit.h"
class BookComp : public QWidget
{
	Q_OBJECT

public:
	BookComp(QWidget *parent = Q_NULLPTR);
	~BookComp();
	
public slots:
	bool SqlQuery(char* SqlContent = const_cast < char*>("SELECT B_name,B_writer,B_publish,B_price,B_Class,Num_total,Num_remain FROM Book,Classfy WHERE Book.Class_no=Classfy.Class_no"));
	bool Check();
	bool Add();
	bool Update();
	bool Delete();
	void Select();
public:
	Ui::BookComp ui;
private:
	
	Book_Edit Be;
	BookEdit_Comp BEc;
	bool flag = true;
};
