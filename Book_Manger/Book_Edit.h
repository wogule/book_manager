#pragma once

#include <QDialog>
#include "ui_Book_Edit.h"
#include "DBHelper.h"
class Book_Edit : public QDialog
{
	Q_OBJECT

public:
	Book_Edit(QWidget *parent = Q_NULLPTR);
	~Book_Edit();
public slots:
	
signals:
	
public:
	Ui::Book_Edit ui;
	
};
