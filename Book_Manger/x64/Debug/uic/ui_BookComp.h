/********************************************************************************
** Form generated from reading UI file 'BookComp.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOOKCOMP_H
#define UI_BOOKCOMP_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BookComp
{
public:
    QHBoxLayout *horizontalLayout_2;
    QWidget *widget;
    QGridLayout *gridLayout;
    QPushButton *All_Btn;
    QTableView *View;
    QLineEdit *S_led;
    QComboBox *Classes_comb;
    QWidget *widget_3;
    QVBoxLayout *verticalLayout;
    QWidget *widget_4;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *B_Led;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *Add_Btn;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *Update_Btn;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *Delete_Btn;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *BookComp)
    {
        if (BookComp->objectName().isEmpty())
            BookComp->setObjectName(QString::fromUtf8("BookComp"));
        BookComp->resize(1200, 840);
        BookComp->setMinimumSize(QSize(1200, 840));
        BookComp->setMaximumSize(QSize(1200, 850));
        BookComp->setStyleSheet(QString::fromUtf8("QPushButton#Add_Btn{\n"
"	border-image: url(:/icon/jia.png);\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"QPushButton#Update_Btn{\n"
"	border-image: url(:/icon/xiu.png);\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"QPushButton#Delete_Btn{\n"
"	border-image: url(:/icon/shan.png);\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"QPushButton#Add_Btn:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QPushButton#Update_Btn:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QPushButton#Delete_Btn:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QLineEdit{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QPushButton#All_Btn{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"}\n"
"QPushButton#All_Btn:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QComboBox#Classes_comb{\n"
"	background-colo"
                        "r:rgb(255, 254, 240,80);\n"
"}\n"
"QComboBox#Classes_comb:hover{\n"
"	border:2px solid rgb(255,254,240);\n"
"}\n"
"QTableView{\n"
"	background-color:rgb(255, 254, 240,70);\n"
"	border:2px solid rgb(255, 254, 240);\n"
"	border-radius:5px;\n"
"	selection-color: black;\n"
"    selection-background-color: rgb(255, 254, 240);\n"
"}\n"
""));
        horizontalLayout_2 = new QHBoxLayout(BookComp);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        widget = new QWidget(BookComp);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        widget->setMinimumSize(QSize(880, 800));
        widget->setMaximumSize(QSize(12323123, 16777215));
        gridLayout = new QGridLayout(widget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(11, 0, 11, 11);
        All_Btn = new QPushButton(widget);
        All_Btn->setObjectName(QString::fromUtf8("All_Btn"));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font.setPointSize(12);
        All_Btn->setFont(font);

        gridLayout->addWidget(All_Btn, 0, 2, 1, 1);

        View = new QTableView(widget);
        View->setObjectName(QString::fromUtf8("View"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(View->sizePolicy().hasHeightForWidth());
        View->setSizePolicy(sizePolicy1);
        View->setMinimumSize(QSize(0, 0));
        View->setFont(font);

        gridLayout->addWidget(View, 1, 0, 1, 3);

        S_led = new QLineEdit(widget);
        S_led->setObjectName(QString::fromUtf8("S_led"));
        S_led->setFont(font);

        gridLayout->addWidget(S_led, 0, 0, 1, 1);

        Classes_comb = new QComboBox(widget);
        Classes_comb->addItem(QString());
        Classes_comb->addItem(QString());
        Classes_comb->setObjectName(QString::fromUtf8("Classes_comb"));
        Classes_comb->setFont(font);

        gridLayout->addWidget(Classes_comb, 0, 1, 1, 1);


        horizontalLayout_2->addWidget(widget);

        widget_3 = new QWidget(BookComp);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        sizePolicy1.setHeightForWidth(widget_3->sizePolicy().hasHeightForWidth());
        widget_3->setSizePolicy(sizePolicy1);
        verticalLayout = new QVBoxLayout(widget_3);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(11, 0, 11, 11);
        widget_4 = new QWidget(widget_3);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        horizontalLayout_3 = new QHBoxLayout(widget_4);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 11, 0, 0);
        B_Led = new QLineEdit(widget_4);
        B_Led->setObjectName(QString::fromUtf8("B_Led"));
        B_Led->setFont(font);

        horizontalLayout_3->addWidget(B_Led);


        verticalLayout->addWidget(widget_4);

        widget_2 = new QWidget(widget_3);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        horizontalLayout = new QHBoxLayout(widget_2);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        Add_Btn = new QPushButton(widget_2);
        Add_Btn->setObjectName(QString::fromUtf8("Add_Btn"));
        sizePolicy.setHeightForWidth(Add_Btn->sizePolicy().hasHeightForWidth());
        Add_Btn->setSizePolicy(sizePolicy);
        Add_Btn->setMinimumSize(QSize(25, 25));
        Add_Btn->setMaximumSize(QSize(25, 25));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font1.setPointSize(16);
        Add_Btn->setFont(font1);
        Add_Btn->setAutoDefault(false);
        Add_Btn->setFlat(false);

        horizontalLayout->addWidget(Add_Btn);

        horizontalSpacer_3 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        Update_Btn = new QPushButton(widget_2);
        Update_Btn->setObjectName(QString::fromUtf8("Update_Btn"));
        sizePolicy.setHeightForWidth(Update_Btn->sizePolicy().hasHeightForWidth());
        Update_Btn->setSizePolicy(sizePolicy);
        Update_Btn->setMinimumSize(QSize(25, 25));
        Update_Btn->setMaximumSize(QSize(25, 25));
        Update_Btn->setFont(font1);

        horizontalLayout->addWidget(Update_Btn);

        horizontalSpacer_4 = new QSpacerItem(30, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        Delete_Btn = new QPushButton(widget_2);
        Delete_Btn->setObjectName(QString::fromUtf8("Delete_Btn"));
        sizePolicy.setHeightForWidth(Delete_Btn->sizePolicy().hasHeightForWidth());
        Delete_Btn->setSizePolicy(sizePolicy);
        Delete_Btn->setMinimumSize(QSize(25, 25));
        Delete_Btn->setMaximumSize(QSize(25, 25));
        Delete_Btn->setFont(font1);

        horizontalLayout->addWidget(Delete_Btn);


        verticalLayout->addWidget(widget_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout_2->addWidget(widget_3);


        retranslateUi(BookComp);

        Add_Btn->setDefault(false);


        QMetaObject::connectSlotsByName(BookComp);
    } // setupUi

    void retranslateUi(QWidget *BookComp)
    {
        BookComp->setWindowTitle(QCoreApplication::translate("BookComp", "BookComp", nullptr));
        All_Btn->setText(QCoreApplication::translate("BookComp", "\346\230\276\347\244\272\345\205\250\351\203\250", nullptr));
        Classes_comb->setItemText(0, QCoreApplication::translate("BookComp", "\344\271\246\345\220\215", nullptr));
        Classes_comb->setItemText(1, QCoreApplication::translate("BookComp", "\347\261\273\345\210\253", nullptr));

        Add_Btn->setText(QString());
        Update_Btn->setText(QString());
        Delete_Btn->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class BookComp: public Ui_BookComp {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOOKCOMP_H
