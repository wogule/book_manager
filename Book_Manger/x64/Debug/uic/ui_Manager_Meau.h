/********************************************************************************
** Form generated from reading UI file 'Manager_Meau.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MANAGER_MEAU_H
#define UI_MANAGER_MEAU_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Manager_Meau
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QPushButton *Back_Btn;
    QSpacerItem *horizontalSpacer;
    QLabel *M_lab;
    QSpacerItem *horizontalSpacer_2;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *Classfy_Btn;
    QSpacerItem *horizontalSpacer_3;
    QWidget *widget_6;
    QHBoxLayout *horizontalLayout_6;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *Fine_Btn;
    QSpacerItem *horizontalSpacer_6;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_7;
    QPushButton *Book_Btn;
    QSpacerItem *horizontalSpacer_8;
    QWidget *widget_4;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_9;
    QPushButton *Borrow_Btn;
    QSpacerItem *horizontalSpacer_10;
    QWidget *widget_5;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_11;
    QPushButton *Read_Btn;
    QSpacerItem *horizontalSpacer_12;

    void setupUi(QDialog *Manager_Meau)
    {
        if (Manager_Meau->objectName().isEmpty())
            Manager_Meau->setObjectName(QString::fromUtf8("Manager_Meau"));
        Manager_Meau->resize(800, 735);
        Manager_Meau->setMinimumSize(QSize(800, 735));
        Manager_Meau->setMaximumSize(QSize(800, 735));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Manager_Meau->setWindowIcon(icon);
        Manager_Meau->setStyleSheet(QString::fromUtf8("QDialog{\n"
"	border-image: url(:/06.jpg);\n"
"}\n"
"QPushButton{\n"
"	background-color:transparent;\n"
"	border:2px solid rgb(255, 254, 240);\n"
"	border-radius:15px;\n"
"}\n"
"QPushButton:hover{\n"
"	border:5px solid rgb(255, 254, 240);\n"
"}\n"
"QPushButton:pressed{\n"
"	background-color: rgb(255, 254, 240);\n"
"}"));
        verticalLayout = new QVBoxLayout(Manager_Meau);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        widget = new QWidget(Manager_Meau);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Back_Btn = new QPushButton(widget);
        Back_Btn->setObjectName(QString::fromUtf8("Back_Btn"));
        QFont font;
        font.setFamily(QString::fromUtf8("Adobe Arabic"));
        font.setPointSize(16);
        Back_Btn->setFont(font);

        horizontalLayout->addWidget(Back_Btn);

        horizontalSpacer = new QSpacerItem(155, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        M_lab = new QLabel(widget);
        M_lab->setObjectName(QString::fromUtf8("M_lab"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font1.setPointSize(26);
        M_lab->setFont(font1);
        M_lab->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(M_lab);

        horizontalSpacer_2 = new QSpacerItem(260, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addWidget(widget);

        widget_2 = new QWidget(Manager_Meau);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        sizePolicy.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
        widget_2->setSizePolicy(sizePolicy);
        horizontalLayout_2 = new QHBoxLayout(widget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);

        Classfy_Btn = new QPushButton(widget_2);
        Classfy_Btn->setObjectName(QString::fromUtf8("Classfy_Btn"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font2.setPointSize(22);
        Classfy_Btn->setFont(font2);

        horizontalLayout_2->addWidget(Classfy_Btn);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);


        verticalLayout->addWidget(widget_2);

        widget_6 = new QWidget(Manager_Meau);
        widget_6->setObjectName(QString::fromUtf8("widget_6"));
        horizontalLayout_6 = new QHBoxLayout(widget_6);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_5);

        Fine_Btn = new QPushButton(widget_6);
        Fine_Btn->setObjectName(QString::fromUtf8("Fine_Btn"));
        Fine_Btn->setFont(font2);

        horizontalLayout_6->addWidget(Fine_Btn);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_6);


        verticalLayout->addWidget(widget_6);

        widget_3 = new QWidget(Manager_Meau);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        horizontalLayout_3 = new QHBoxLayout(widget_3);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_7);

        Book_Btn = new QPushButton(widget_3);
        Book_Btn->setObjectName(QString::fromUtf8("Book_Btn"));
        Book_Btn->setFont(font2);

        horizontalLayout_3->addWidget(Book_Btn);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_8);


        verticalLayout->addWidget(widget_3);

        widget_4 = new QWidget(Manager_Meau);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        horizontalLayout_4 = new QHBoxLayout(widget_4);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_9);

        Borrow_Btn = new QPushButton(widget_4);
        Borrow_Btn->setObjectName(QString::fromUtf8("Borrow_Btn"));
        Borrow_Btn->setFont(font2);

        horizontalLayout_4->addWidget(Borrow_Btn);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_10);


        verticalLayout->addWidget(widget_4);

        widget_5 = new QWidget(Manager_Meau);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));
        horizontalLayout_5 = new QHBoxLayout(widget_5);
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_11);

        Read_Btn = new QPushButton(widget_5);
        Read_Btn->setObjectName(QString::fromUtf8("Read_Btn"));
        Read_Btn->setFont(font2);

        horizontalLayout_5->addWidget(Read_Btn);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_12);


        verticalLayout->addWidget(widget_5);


        retranslateUi(Manager_Meau);

        QMetaObject::connectSlotsByName(Manager_Meau);
    } // setupUi

    void retranslateUi(QDialog *Manager_Meau)
    {
        Manager_Meau->setWindowTitle(QCoreApplication::translate("Manager_Meau", "\347\256\241\347\220\206\345\221\230\350\217\234\345\215\225", nullptr));
        Back_Btn->setText(QCoreApplication::translate("Manager_Meau", "\350\277\224\345\233\236", nullptr));
        M_lab->setText(QCoreApplication::translate("Manager_Meau", "\347\256\241\347\220\206\345\221\230\350\217\234\345\215\225", nullptr));
        Classfy_Btn->setText(QCoreApplication::translate("Manager_Meau", "\347\261\273\345\210\253\344\277\241\346\201\257\347\256\241\347\220\206", nullptr));
        Fine_Btn->setText(QCoreApplication::translate("Manager_Meau", "\347\275\232\351\207\221\345\210\227\350\241\250\344\270\200\350\247\210", nullptr));
        Book_Btn->setText(QCoreApplication::translate("Manager_Meau", "\345\233\276\344\271\246\344\277\241\346\201\257\347\256\241\347\220\206", nullptr));
        Borrow_Btn->setText(QCoreApplication::translate("Manager_Meau", "\345\200\237\351\230\205\344\277\241\346\201\257\347\256\241\347\220\206", nullptr));
        Read_Btn->setText(QCoreApplication::translate("Manager_Meau", "\350\257\273\350\200\205\344\277\241\346\201\257\347\256\241\347\220\206", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Manager_Meau: public Ui_Manager_Meau {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MANAGER_MEAU_H
