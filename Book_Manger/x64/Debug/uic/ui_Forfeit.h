/********************************************************************************
** Form generated from reading UI file 'Forfeit.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORFEIT_H
#define UI_FORFEIT_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Forfeit
{
public:
    QVBoxLayout *verticalLayout_2;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QTableView *Fine_View;
    QPushButton *Back_Btn;

    void setupUi(QDialog *Forfeit)
    {
        if (Forfeit->objectName().isEmpty())
            Forfeit->setObjectName(QString::fromUtf8("Forfeit"));
        Forfeit->resize(590, 613);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Forfeit->setWindowIcon(icon);
        Forfeit->setStyleSheet(QString::fromUtf8("QDialog{\n"
"	\n"
"	border-image: url(:/08.png);\n"
"}\n"
"QPushButton{\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"\n"
"QTableView{\n"
"	background-color:rgb(255, 254, 240,70);\n"
"	border:2px solid rgb(255, 254, 240);\n"
"	border-radius:5px;\n"
"	selection-color: black;\n"
"    selection-background-color: rgb(255, 254, 240);\n"
"}\n"
""));
        verticalLayout_2 = new QVBoxLayout(Forfeit);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        widget = new QWidget(Forfeit);
        widget->setObjectName(QString::fromUtf8("widget"));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        Fine_View = new QTableView(widget);
        Fine_View->setObjectName(QString::fromUtf8("Fine_View"));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font.setPointSize(16);
        Fine_View->setFont(font);

        verticalLayout->addWidget(Fine_View);

        Back_Btn = new QPushButton(widget);
        Back_Btn->setObjectName(QString::fromUtf8("Back_Btn"));
        Back_Btn->setFont(font);

        verticalLayout->addWidget(Back_Btn);


        verticalLayout_2->addWidget(widget);


        retranslateUi(Forfeit);

        QMetaObject::connectSlotsByName(Forfeit);
    } // setupUi

    void retranslateUi(QDialog *Forfeit)
    {
        Forfeit->setWindowTitle(QCoreApplication::translate("Forfeit", "\350\264\271\347\224\250", nullptr));
        Back_Btn->setText(QCoreApplication::translate("Forfeit", "\350\277\224\345\233\236", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Forfeit: public Ui_Forfeit {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORFEIT_H
