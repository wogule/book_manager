/********************************************************************************
** Form generated from reading UI file 'Borrow_Book.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BORROW_BOOK_H
#define UI_BORROW_BOOK_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Borrow_Book
{
public:
    QHBoxLayout *horizontalLayout_2;
    QWidget *widget_3;
    QVBoxLayout *verticalLayout;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout;
    QLineEdit *Book_Led;
    QComboBox *Class_Comb;
    QPushButton *All_Btn;
    QTableView *Book_View;
    QWidget *widget;
    QGridLayout *gridLayout;
    QLineEdit *Date_Led;
    QLabel *label;
    QLabel *label_2;
    QPushButton *Back_Btn;
    QPushButton *Bor_Btn;
    QLineEdit *Bname_Led;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *Borrow_Book)
    {
        if (Borrow_Book->objectName().isEmpty())
            Borrow_Book->setObjectName(QString::fromUtf8("Borrow_Book"));
        Borrow_Book->resize(934, 865);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Borrow_Book->setWindowIcon(icon);
        Borrow_Book->setStyleSheet(QString::fromUtf8("QDialog{\n"
"	\n"
"	border-image: url(:/02.jpg);\n"
"}\n"
"QPushButton{\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"\n"
"QLineEdit{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QLabel{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QPushButton#All_Btn{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"}\n"
"QPushButton#All_Btn:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QComboBox#Class_Comb{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"QComboBox#Class_Comb:hover{\n"
"	border:2px solid rgb(255,254,240);\n"
"}\n"
"QTableView{\n"
"	background-color:rgb(255, 254, 240,70);\n"
"	border:2px solid rgb(255, 254, 240);\n"
"	border-radius:5px;\n"
"	selection-color: black;\n"
"    selection-background-color: rgb(255, 2"
                        "54, 240);\n"
"}\n"
""));
        horizontalLayout_2 = new QHBoxLayout(Borrow_Book);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        widget_3 = new QWidget(Borrow_Book);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        verticalLayout = new QVBoxLayout(widget_3);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        widget_2 = new QWidget(widget_3);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        horizontalLayout = new QHBoxLayout(widget_2);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        Book_Led = new QLineEdit(widget_2);
        Book_Led->setObjectName(QString::fromUtf8("Book_Led"));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font.setPointSize(16);
        Book_Led->setFont(font);

        horizontalLayout->addWidget(Book_Led);

        Class_Comb = new QComboBox(widget_2);
        Class_Comb->addItem(QString());
        Class_Comb->addItem(QString());
        Class_Comb->setObjectName(QString::fromUtf8("Class_Comb"));
        Class_Comb->setFont(font);

        horizontalLayout->addWidget(Class_Comb);

        All_Btn = new QPushButton(widget_2);
        All_Btn->setObjectName(QString::fromUtf8("All_Btn"));
        All_Btn->setFont(font);

        horizontalLayout->addWidget(All_Btn);


        verticalLayout->addWidget(widget_2);

        Book_View = new QTableView(widget_3);
        Book_View->setObjectName(QString::fromUtf8("Book_View"));
        Book_View->setFont(font);

        verticalLayout->addWidget(Book_View);


        horizontalLayout_2->addWidget(widget_3);

        widget = new QWidget(Borrow_Book);
        widget->setObjectName(QString::fromUtf8("widget"));
        gridLayout = new QGridLayout(widget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        Date_Led = new QLineEdit(widget);
        Date_Led->setObjectName(QString::fromUtf8("Date_Led"));
        Date_Led->setFont(font);

        gridLayout->addWidget(Date_Led, 1, 1, 1, 1);

        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        Back_Btn = new QPushButton(widget);
        Back_Btn->setObjectName(QString::fromUtf8("Back_Btn"));
        Back_Btn->setFont(font);

        gridLayout->addWidget(Back_Btn, 2, 0, 1, 1);

        Bor_Btn = new QPushButton(widget);
        Bor_Btn->setObjectName(QString::fromUtf8("Bor_Btn"));
        Bor_Btn->setFont(font);

        gridLayout->addWidget(Bor_Btn, 2, 1, 1, 1);

        Bname_Led = new QLineEdit(widget);
        Bname_Led->setObjectName(QString::fromUtf8("Bname_Led"));
        Bname_Led->setFont(font);

        gridLayout->addWidget(Bname_Led, 0, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 3, 0, 1, 1);


        horizontalLayout_2->addWidget(widget);


        retranslateUi(Borrow_Book);

        QMetaObject::connectSlotsByName(Borrow_Book);
    } // setupUi

    void retranslateUi(QDialog *Borrow_Book)
    {
        Borrow_Book->setWindowTitle(QCoreApplication::translate("Borrow_Book", "\345\200\237\344\271\246", nullptr));
        Class_Comb->setItemText(0, QCoreApplication::translate("Borrow_Book", "\344\271\246\345\220\215", nullptr));
        Class_Comb->setItemText(1, QCoreApplication::translate("Borrow_Book", "\347\261\273\345\210\253", nullptr));

        All_Btn->setText(QCoreApplication::translate("Borrow_Book", "\346\230\276\347\244\272\345\205\250\351\203\250", nullptr));
        Date_Led->setText(QCoreApplication::translate("Borrow_Book", "30", nullptr));
        label->setText(QCoreApplication::translate("Borrow_Book", "\346\211\200\345\200\237\344\271\246\345\220\215\357\274\232", nullptr));
        label_2->setText(QCoreApplication::translate("Borrow_Book", "\346\211\200\345\200\237\345\244\251\346\225\260\357\274\232", nullptr));
        Back_Btn->setText(QCoreApplication::translate("Borrow_Book", "\350\277\224\345\233\236", nullptr));
        Bor_Btn->setText(QCoreApplication::translate("Borrow_Book", "\345\200\237\344\271\246", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Borrow_Book: public Ui_Borrow_Book {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BORROW_BOOK_H
