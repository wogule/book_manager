/********************************************************************************
** Form generated from reading UI file 'Back_Book.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BACK_BOOK_H
#define UI_BACK_BOOK_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Back_Book
{
public:
    QHBoxLayout *horizontalLayout_4;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout_2;
    QTableView *Back_View;
    QWidget *widget_4;
    QVBoxLayout *verticalLayout;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *BackBook_Led;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *Back_Btn;
    QPushButton *BackBook_Btn;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *Back_Book)
    {
        if (Back_Book->objectName().isEmpty())
            Back_Book->setObjectName(QString::fromUtf8("Back_Book"));
        Back_Book->resize(611, 651);
        QFont font;
        font.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font.setPointSize(16);
        Back_Book->setFont(font);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Back_Book->setWindowIcon(icon);
        Back_Book->setStyleSheet(QString::fromUtf8("QDialog{\n"
"	\n"
"	border-image: url(:/02.jpg);\n"
"}\n"
"QPushButton{\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"\n"
"QLineEdit{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QLabel{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"\n"
"QTableView{\n"
"	background-color:rgb(255, 254, 240,70);\n"
"	border:2px solid rgb(255, 254, 240);\n"
"	border-radius:5px;\n"
"	selection-color: black;\n"
"    selection-background-color: rgb(255, 254, 240);\n"
"}\n"
""));
        horizontalLayout_4 = new QHBoxLayout(Back_Book);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        widget_2 = new QWidget(Back_Book);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        horizontalLayout_2 = new QHBoxLayout(widget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        Back_View = new QTableView(widget_2);
        Back_View->setObjectName(QString::fromUtf8("Back_View"));
        Back_View->setFont(font);

        horizontalLayout_2->addWidget(Back_View);


        horizontalLayout_4->addWidget(widget_2);

        widget_4 = new QWidget(Back_Book);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        verticalLayout = new QVBoxLayout(widget_4);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        widget = new QWidget(widget_4);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        horizontalLayout->addWidget(label);

        BackBook_Led = new QLineEdit(widget);
        BackBook_Led->setObjectName(QString::fromUtf8("BackBook_Led"));
        BackBook_Led->setFont(font);
        BackBook_Led->setReadOnly(true);

        horizontalLayout->addWidget(BackBook_Led);


        verticalLayout->addWidget(widget);

        widget_3 = new QWidget(widget_4);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        sizePolicy.setHeightForWidth(widget_3->sizePolicy().hasHeightForWidth());
        widget_3->setSizePolicy(sizePolicy);
        horizontalLayout_3 = new QHBoxLayout(widget_3);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        Back_Btn = new QPushButton(widget_3);
        Back_Btn->setObjectName(QString::fromUtf8("Back_Btn"));
        Back_Btn->setFont(font);

        horizontalLayout_3->addWidget(Back_Btn);

        BackBook_Btn = new QPushButton(widget_3);
        BackBook_Btn->setObjectName(QString::fromUtf8("BackBook_Btn"));
        BackBook_Btn->setFont(font);

        horizontalLayout_3->addWidget(BackBook_Btn);


        verticalLayout->addWidget(widget_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout_4->addWidget(widget_4);


        retranslateUi(Back_Book);

        QMetaObject::connectSlotsByName(Back_Book);
    } // setupUi

    void retranslateUi(QDialog *Back_Book)
    {
        Back_Book->setWindowTitle(QCoreApplication::translate("Back_Book", "\350\277\230\344\271\246", nullptr));
        label->setText(QCoreApplication::translate("Back_Book", "\346\211\200\350\277\230\344\271\246\345\220\215\357\274\232", nullptr));
        Back_Btn->setText(QCoreApplication::translate("Back_Book", "\350\277\224\345\233\236", nullptr));
        BackBook_Btn->setText(QCoreApplication::translate("Back_Book", "\350\277\230\344\271\246", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Back_Book: public Ui_Back_Book {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BACK_BOOK_H
