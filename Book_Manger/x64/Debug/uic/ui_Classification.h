/********************************************************************************
** Form generated from reading UI file 'Classification.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLASSIFICATION_H
#define UI_CLASSIFICATION_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Classification
{
public:
    QVBoxLayout *verticalLayout_2;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QTableView *C_View;
    QWidget *widget_3;
    QGridLayout *gridLayout;
    QSpacerItem *verticalSpacer;
    QLineEdit *Class_Led;
    QPushButton *Back_Btn;
    QPushButton *Add_Btn;
    QPushButton *Delete_Btn;
    QLabel *label;
    QLabel *label_2;
    QPushButton *Update_Btn;
    QLineEdit *Num_Led;

    void setupUi(QDialog *Classification)
    {
        if (Classification->objectName().isEmpty())
            Classification->setObjectName(QString::fromUtf8("Classification"));
        Classification->resize(640, 520);
        Classification->setMinimumSize(QSize(640, 520));
        Classification->setMaximumSize(QSize(640, 520));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Classification->setWindowIcon(icon);
        Classification->setStyleSheet(QString::fromUtf8("QDialog{\n"
"	\n"
"	border-image: url(:/07.jpg);\n"
"}\n"
"QPushButton#Add_Btn{\n"
"	border-image: url(:/icon/jia.png);\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"QPushButton#Update_Btn{\n"
"	border-image: url(:/icon/xiu.png);\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"QPushButton#Delete_Btn{\n"
"	border-image: url(:/icon/shan.png);\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"QPushButton#Add_Btn:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QPushButton#Update_Btn:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QPushButton#Delete_Btn:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"\n"
"QTableView{\n"
"	background-color:rgb(255, 254, 240,70);\n"
"	border:2px solid rgb(255, 254, 240);\n"
"	border-radius:5px;\n"
"	selection-color: black;\n"
"    selection-background-color: rgb(255, 254, 240);\n"
"}\n"
"QPushButton#Back_Btn{\n"
"	border-image: url(:/icon/back.png);\n"
"	borde"
                        "r-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"QPushButton#Back_Btn:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QLineEdit{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border:2px solid rgb(255, 254, 240);\n"
"	border-radius:5px;\n"
"}\n"
"QLabel{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"}"));
        verticalLayout_2 = new QVBoxLayout(Classification);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        widget = new QWidget(Classification);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        C_View = new QTableView(widget);
        C_View->setObjectName(QString::fromUtf8("C_View"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(C_View->sizePolicy().hasHeightForWidth());
        C_View->setSizePolicy(sizePolicy1);
        QFont font;
        font.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font.setPointSize(16);
        C_View->setFont(font);

        horizontalLayout->addWidget(C_View);

        widget_3 = new QWidget(widget);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        gridLayout = new QGridLayout(widget_3);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 3, 0, 1, 1);

        Class_Led = new QLineEdit(widget_3);
        Class_Led->setObjectName(QString::fromUtf8("Class_Led"));
        Class_Led->setFont(font);

        gridLayout->addWidget(Class_Led, 1, 1, 1, 3);

        Back_Btn = new QPushButton(widget_3);
        Back_Btn->setObjectName(QString::fromUtf8("Back_Btn"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(Back_Btn->sizePolicy().hasHeightForWidth());
        Back_Btn->setSizePolicy(sizePolicy2);
        Back_Btn->setMinimumSize(QSize(25, 25));
        Back_Btn->setMaximumSize(QSize(25, 25));

        gridLayout->addWidget(Back_Btn, 2, 0, 1, 1);

        Add_Btn = new QPushButton(widget_3);
        Add_Btn->setObjectName(QString::fromUtf8("Add_Btn"));
        sizePolicy2.setHeightForWidth(Add_Btn->sizePolicy().hasHeightForWidth());
        Add_Btn->setSizePolicy(sizePolicy2);
        Add_Btn->setMinimumSize(QSize(25, 25));
        Add_Btn->setMaximumSize(QSize(25, 25));

        gridLayout->addWidget(Add_Btn, 2, 1, 1, 1);

        Delete_Btn = new QPushButton(widget_3);
        Delete_Btn->setObjectName(QString::fromUtf8("Delete_Btn"));
        sizePolicy2.setHeightForWidth(Delete_Btn->sizePolicy().hasHeightForWidth());
        Delete_Btn->setSizePolicy(sizePolicy2);
        Delete_Btn->setMinimumSize(QSize(25, 25));
        Delete_Btn->setMaximumSize(QSize(25, 25));

        gridLayout->addWidget(Delete_Btn, 2, 3, 1, 1);

        label = new QLabel(widget_3);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font);

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(widget_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);

        gridLayout->addWidget(label_2, 1, 0, 1, 1);

        Update_Btn = new QPushButton(widget_3);
        Update_Btn->setObjectName(QString::fromUtf8("Update_Btn"));
        sizePolicy2.setHeightForWidth(Update_Btn->sizePolicy().hasHeightForWidth());
        Update_Btn->setSizePolicy(sizePolicy2);
        Update_Btn->setMinimumSize(QSize(25, 25));
        Update_Btn->setMaximumSize(QSize(25, 25));

        gridLayout->addWidget(Update_Btn, 2, 2, 1, 1);

        Num_Led = new QLineEdit(widget_3);
        Num_Led->setObjectName(QString::fromUtf8("Num_Led"));
        Num_Led->setFont(font);

        gridLayout->addWidget(Num_Led, 0, 1, 1, 3);


        horizontalLayout->addWidget(widget_3);


        verticalLayout_2->addWidget(widget);


        retranslateUi(Classification);

        QMetaObject::connectSlotsByName(Classification);
    } // setupUi

    void retranslateUi(QDialog *Classification)
    {
        Classification->setWindowTitle(QCoreApplication::translate("Classification", "\347\261\273\345\210\253", nullptr));
        Back_Btn->setText(QString());
        Add_Btn->setText(QString());
        Delete_Btn->setText(QString());
        label->setText(QCoreApplication::translate("Classification", "\345\272\217\345\217\267\357\274\232", nullptr));
        label_2->setText(QCoreApplication::translate("Classification", "\347\261\273\345\220\215\357\274\232", nullptr));
        Update_Btn->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Classification: public Ui_Classification {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLASSIFICATION_H
