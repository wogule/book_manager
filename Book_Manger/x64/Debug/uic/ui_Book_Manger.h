/********************************************************************************
** Form generated from reading UI file 'Book_Manger.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOOK_MANGER_H
#define UI_BOOK_MANGER_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCommandLinkButton>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Book_MangerClass
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *widget;
    QGridLayout *gridLayout;
    QLineEdit *U_ledit;
    QLineEdit *P_ledit;
    QSpacerItem *horizontalSpacer;
    QLabel *U_lab;
    QSpacerItem *horizontalSpacer_2;
    QLabel *P_lab;
    QSpacerItem *verticalSpacer;
    QWidget *widget_4;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_8;
    QRadioButton *RBtn_U;
    QSpacerItem *horizontalSpacer_10;
    QRadioButton *RBtn_A;
    QSpacerItem *horizontalSpacer_9;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *Btn_OK;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *Btn_NO;
    QSpacerItem *horizontalSpacer_4;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_6;
    QCommandLinkButton *LBtn_R;
    QSpacerItem *horizontalSpacer_7;

    void setupUi(QDialog *Book_MangerClass)
    {
        if (Book_MangerClass->objectName().isEmpty())
            Book_MangerClass->setObjectName(QString::fromUtf8("Book_MangerClass"));
        Book_MangerClass->resize(600, 430);
        Book_MangerClass->setMinimumSize(QSize(600, 430));
        Book_MangerClass->setMaximumSize(QSize(600, 430));
        QFont font;
        font.setFamily(QString::fromUtf8("\346\245\267\344\275\223"));
        font.setPointSize(10);
        Book_MangerClass->setFont(font);
        Book_MangerClass->setContextMenuPolicy(Qt::DefaultContextMenu);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Book_MangerClass->setWindowIcon(icon);
        Book_MangerClass->setStyleSheet(QString::fromUtf8("QDialog{\n"
"	background-color: rgb(255, 254, 240);\n"
"	border-image: url(:/01.jpg);\n"
"}\n"
"\n"
"QLineEdit{\n"
" \n"
"	border:2px solid #7b7b7b;\n"
"	border-radius:15px;\n"
"	background-color:#fffef0\n"
"}\n"
"QLabel{\n"
"       \n"
"color: rgb(0, 0, 0);     \n"
"\n"
"}\n"
"QPushButton{\n"
"	background-color: rgb(255, 254, 240);\n"
"}\n"
"\n"
"QCommandLinkButton{\n"
"	color: rgb(255, 254, 240);\n"
"}\n"
"QCommandLinkButton:hover{\n"
"	background-color:transparent;\n"
"	\n"
"}\n"
"QRadioButton{\n"
"	color: rgb(255, 254, 240);\n"
"}"));
        verticalLayout = new QVBoxLayout(Book_MangerClass);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        widget = new QWidget(Book_MangerClass);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(widget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        U_ledit = new QLineEdit(widget);
        U_ledit->setObjectName(QString::fromUtf8("U_ledit"));
        U_ledit->setMaximumSize(QSize(500, 400));
        QFont font1;
        font1.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        U_ledit->setFont(font1);
        U_ledit->setContextMenuPolicy(Qt::NoContextMenu);

        gridLayout->addWidget(U_ledit, 0, 2, 1, 1);

        P_ledit = new QLineEdit(widget);
        P_ledit->setObjectName(QString::fromUtf8("P_ledit"));
        P_ledit->setMaximumSize(QSize(500, 400));
        QPalette palette;
        QBrush brush(QColor(255, 254, 240, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush);
        palette.setBrush(QPalette::Active, QPalette::Window, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush);
        P_ledit->setPalette(palette);
        P_ledit->setFont(font1);
        P_ledit->setContextMenuPolicy(Qt::NoContextMenu);
        P_ledit->setAcceptDrops(true);
        P_ledit->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(P_ledit, 2, 2, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 0, 1, 1);

        U_lab = new QLabel(widget);
        U_lab->setObjectName(QString::fromUtf8("U_lab"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\226\260\351\255\217"));
        font2.setPointSize(16);
        font2.setBold(false);
        font2.setWeight(50);
        U_lab->setFont(font2);

        gridLayout->addWidget(U_lab, 0, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 3, 1, 1);

        P_lab = new QLabel(widget);
        P_lab->setObjectName(QString::fromUtf8("P_lab"));
        P_lab->setFont(font2);

        gridLayout->addWidget(P_lab, 2, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer, 1, 2, 1, 1);


        verticalLayout->addWidget(widget);

        widget_4 = new QWidget(Book_MangerClass);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        sizePolicy.setHeightForWidth(widget_4->sizePolicy().hasHeightForWidth());
        widget_4->setSizePolicy(sizePolicy);
        horizontalLayout_3 = new QHBoxLayout(widget_4);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_8);

        RBtn_U = new QRadioButton(widget_4);
        RBtn_U->setObjectName(QString::fromUtf8("RBtn_U"));
        RBtn_U->setChecked(true);

        horizontalLayout_3->addWidget(RBtn_U);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_10);

        RBtn_A = new QRadioButton(widget_4);
        RBtn_A->setObjectName(QString::fromUtf8("RBtn_A"));

        horizontalLayout_3->addWidget(RBtn_A);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_9);


        verticalLayout->addWidget(widget_4);

        widget_2 = new QWidget(Book_MangerClass);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        sizePolicy.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
        widget_2->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(widget_2);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        Btn_OK = new QPushButton(widget_2);
        Btn_OK->setObjectName(QString::fromUtf8("Btn_OK"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font3.setPointSize(14);
        Btn_OK->setFont(font3);
        Btn_OK->setCheckable(false);

        horizontalLayout->addWidget(Btn_OK);

        horizontalSpacer_5 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_5);

        Btn_NO = new QPushButton(widget_2);
        Btn_NO->setObjectName(QString::fromUtf8("Btn_NO"));
        Btn_NO->setFont(font3);

        horizontalLayout->addWidget(Btn_NO);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);


        verticalLayout->addWidget(widget_2);

        widget_3 = new QWidget(Book_MangerClass);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        sizePolicy.setHeightForWidth(widget_3->sizePolicy().hasHeightForWidth());
        widget_3->setSizePolicy(sizePolicy);
        horizontalLayout_2 = new QHBoxLayout(widget_3);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);

        LBtn_R = new QCommandLinkButton(widget_3);
        LBtn_R->setObjectName(QString::fromUtf8("LBtn_R"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(LBtn_R->sizePolicy().hasHeightForWidth());
        LBtn_R->setSizePolicy(sizePolicy1);
        QFont font4;
        font4.setFamily(QString::fromUtf8("Segoe UI"));
        font4.setPointSize(8);
        LBtn_R->setFont(font4);
        LBtn_R->setAcceptDrops(false);
        LBtn_R->setLayoutDirection(Qt::LeftToRight);
        LBtn_R->setStyleSheet(QString::fromUtf8(""));
        LBtn_R->setIconSize(QSize(0, 0));
        LBtn_R->setAutoDefault(false);
        LBtn_R->setDefault(false);

        horizontalLayout_2->addWidget(LBtn_R);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_7);


        verticalLayout->addWidget(widget_3);


        retranslateUi(Book_MangerClass);

        QMetaObject::connectSlotsByName(Book_MangerClass);
    } // setupUi

    void retranslateUi(QDialog *Book_MangerClass)
    {
        Book_MangerClass->setWindowTitle(QCoreApplication::translate("Book_MangerClass", "\347\224\250\346\210\267\347\231\273\345\275\225", nullptr));
        U_ledit->setPlaceholderText(QCoreApplication::translate("Book_MangerClass", "\350\257\267\350\276\223\345\205\245\345\233\276\344\271\246\350\257\201\345\217\267", nullptr));
        P_ledit->setPlaceholderText(QCoreApplication::translate("Book_MangerClass", "\350\257\267\350\276\223\345\205\245\345\257\206\347\240\201", nullptr));
        U_lab->setText(QCoreApplication::translate("Book_MangerClass", "\350\264\246\345\217\267\357\274\232", nullptr));
        P_lab->setText(QCoreApplication::translate("Book_MangerClass", "\345\257\206\347\240\201\357\274\232", nullptr));
        RBtn_U->setText(QCoreApplication::translate("Book_MangerClass", "\345\200\237\351\230\205\350\200\205", nullptr));
        RBtn_A->setText(QCoreApplication::translate("Book_MangerClass", "\347\256\241\347\220\206\345\221\230", nullptr));
        Btn_OK->setText(QCoreApplication::translate("Book_MangerClass", "\347\241\256\345\256\232", nullptr));
        Btn_NO->setText(QCoreApplication::translate("Book_MangerClass", "\345\217\226\346\266\210", nullptr));
        LBtn_R->setText(QCoreApplication::translate("Book_MangerClass", "\350\277\230\346\262\241\346\234\211\350\264\246\345\217\267\357\274\237\347\202\271\345\207\273\346\263\250\345\206\214", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Book_MangerClass: public Ui_Book_MangerClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOOK_MANGER_H
