/********************************************************************************
** Form generated from reading UI file 'BookEdit_Comp.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOOKEDIT_COMP_H
#define UI_BOOKEDIT_COMP_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BookEdit_Comp
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *widget;
    QGridLayout *gridLayout;
    QLabel *Name_Lab;
    QLineEdit *Name_Led;
    QLabel *W_Lab;
    QLineEdit *W_Led;
    QLabel *P_Lab;
    QLineEdit *P_Led;
    QLabel *M_Lab;
    QLineEdit *M_Led;
    QLabel *C_Lab;
    QComboBox *C_Comb;
    QLabel *T_Lab;
    QLineEdit *T_Led;
    QLabel *R_Lab;
    QLineEdit *R_Led;

    void setupUi(QWidget *BookEdit_Comp)
    {
        if (BookEdit_Comp->objectName().isEmpty())
            BookEdit_Comp->setObjectName(QString::fromUtf8("BookEdit_Comp"));
        BookEdit_Comp->resize(770, 556);
        BookEdit_Comp->setStyleSheet(QString::fromUtf8("QLabel{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QLineEdit{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QComboBox{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
""));
        verticalLayout = new QVBoxLayout(BookEdit_Comp);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        widget = new QWidget(BookEdit_Comp);
        widget->setObjectName(QString::fromUtf8("widget"));
        gridLayout = new QGridLayout(widget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        Name_Lab = new QLabel(widget);
        Name_Lab->setObjectName(QString::fromUtf8("Name_Lab"));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font.setPointSize(14);
        Name_Lab->setFont(font);

        gridLayout->addWidget(Name_Lab, 0, 0, 1, 1);

        Name_Led = new QLineEdit(widget);
        Name_Led->setObjectName(QString::fromUtf8("Name_Led"));
        Name_Led->setFont(font);

        gridLayout->addWidget(Name_Led, 0, 1, 1, 1);

        W_Lab = new QLabel(widget);
        W_Lab->setObjectName(QString::fromUtf8("W_Lab"));
        W_Lab->setFont(font);

        gridLayout->addWidget(W_Lab, 1, 0, 1, 1);

        W_Led = new QLineEdit(widget);
        W_Led->setObjectName(QString::fromUtf8("W_Led"));
        W_Led->setFont(font);

        gridLayout->addWidget(W_Led, 1, 1, 1, 1);

        P_Lab = new QLabel(widget);
        P_Lab->setObjectName(QString::fromUtf8("P_Lab"));
        P_Lab->setFont(font);

        gridLayout->addWidget(P_Lab, 2, 0, 1, 1);

        P_Led = new QLineEdit(widget);
        P_Led->setObjectName(QString::fromUtf8("P_Led"));
        P_Led->setFont(font);

        gridLayout->addWidget(P_Led, 2, 1, 1, 1);

        M_Lab = new QLabel(widget);
        M_Lab->setObjectName(QString::fromUtf8("M_Lab"));
        M_Lab->setFont(font);

        gridLayout->addWidget(M_Lab, 3, 0, 1, 1);

        M_Led = new QLineEdit(widget);
        M_Led->setObjectName(QString::fromUtf8("M_Led"));
        M_Led->setFont(font);

        gridLayout->addWidget(M_Led, 3, 1, 1, 1);

        C_Lab = new QLabel(widget);
        C_Lab->setObjectName(QString::fromUtf8("C_Lab"));
        C_Lab->setFont(font);

        gridLayout->addWidget(C_Lab, 4, 0, 1, 1);

        C_Comb = new QComboBox(widget);
        C_Comb->setObjectName(QString::fromUtf8("C_Comb"));
        C_Comb->setFont(font);

        gridLayout->addWidget(C_Comb, 4, 1, 1, 1);

        T_Lab = new QLabel(widget);
        T_Lab->setObjectName(QString::fromUtf8("T_Lab"));
        T_Lab->setFont(font);

        gridLayout->addWidget(T_Lab, 5, 0, 1, 1);

        T_Led = new QLineEdit(widget);
        T_Led->setObjectName(QString::fromUtf8("T_Led"));
        T_Led->setFont(font);

        gridLayout->addWidget(T_Led, 5, 1, 1, 1);

        R_Lab = new QLabel(widget);
        R_Lab->setObjectName(QString::fromUtf8("R_Lab"));
        R_Lab->setFont(font);

        gridLayout->addWidget(R_Lab, 6, 0, 1, 1);

        R_Led = new QLineEdit(widget);
        R_Led->setObjectName(QString::fromUtf8("R_Led"));
        R_Led->setFont(font);

        gridLayout->addWidget(R_Led, 6, 1, 1, 1);


        verticalLayout->addWidget(widget);


        retranslateUi(BookEdit_Comp);

        QMetaObject::connectSlotsByName(BookEdit_Comp);
    } // setupUi

    void retranslateUi(QWidget *BookEdit_Comp)
    {
        BookEdit_Comp->setWindowTitle(QCoreApplication::translate("BookEdit_Comp", "BookEdit_Comp", nullptr));
        Name_Lab->setText(QCoreApplication::translate("BookEdit_Comp", "\344\271\246\345\220\215\357\274\232", nullptr));
        W_Lab->setText(QCoreApplication::translate("BookEdit_Comp", "\344\275\234\350\200\205\357\274\232", nullptr));
        P_Lab->setText(QCoreApplication::translate("BookEdit_Comp", "\345\207\272\347\211\210\357\274\232", nullptr));
        M_Lab->setText(QCoreApplication::translate("BookEdit_Comp", "\344\273\267\346\240\274\357\274\232", nullptr));
        C_Lab->setText(QCoreApplication::translate("BookEdit_Comp", "\347\261\273\345\210\253\357\274\232", nullptr));
        T_Lab->setText(QCoreApplication::translate("BookEdit_Comp", "\346\200\273\346\225\260\357\274\232", nullptr));
        R_Lab->setText(QCoreApplication::translate("BookEdit_Comp", "\345\211\251\344\275\231\357\274\232", nullptr));
    } // retranslateUi

};

namespace Ui {
    class BookEdit_Comp: public Ui_BookEdit_Comp {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOOKEDIT_COMP_H
