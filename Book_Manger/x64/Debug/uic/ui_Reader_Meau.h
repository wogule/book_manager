/********************************************************************************
** Form generated from reading UI file 'Reader_Meau.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_READER_MEAU_H
#define UI_READER_MEAU_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Reader_Meau
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *Bor_Btn;
    QSpacerItem *horizontalSpacer_2;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *BackBook_Btn;
    QSpacerItem *horizontalSpacer_4;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *Back_Btn;
    QSpacerItem *horizontalSpacer_6;

    void setupUi(QDialog *Reader_Meau)
    {
        if (Reader_Meau->objectName().isEmpty())
            Reader_Meau->setObjectName(QString::fromUtf8("Reader_Meau"));
        Reader_Meau->resize(400, 385);
        Reader_Meau->setMinimumSize(QSize(400, 385));
        Reader_Meau->setMaximumSize(QSize(400, 385));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Reader_Meau->setWindowIcon(icon);
        Reader_Meau->setStyleSheet(QString::fromUtf8("QDialog{\n"
"	\n"
"	border-image: url(:/08.png);\n"
"}\n"
"QPushButton{\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QPushButton:pressed{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"\n"
""));
        verticalLayout = new QVBoxLayout(Reader_Meau);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        widget = new QWidget(Reader_Meau);
        widget->setObjectName(QString::fromUtf8("widget"));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        Bor_Btn = new QPushButton(widget);
        Bor_Btn->setObjectName(QString::fromUtf8("Bor_Btn"));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font.setPointSize(16);
        Bor_Btn->setFont(font);

        horizontalLayout->addWidget(Bor_Btn);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout->addWidget(widget);

        widget_2 = new QWidget(Reader_Meau);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        horizontalLayout_2 = new QHBoxLayout(widget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_3);

        BackBook_Btn = new QPushButton(widget_2);
        BackBook_Btn->setObjectName(QString::fromUtf8("BackBook_Btn"));
        BackBook_Btn->setFont(font);

        horizontalLayout_2->addWidget(BackBook_Btn);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        verticalLayout->addWidget(widget_2);

        widget_3 = new QWidget(Reader_Meau);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        horizontalLayout_3 = new QHBoxLayout(widget_3);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        Back_Btn = new QPushButton(widget_3);
        Back_Btn->setObjectName(QString::fromUtf8("Back_Btn"));
        Back_Btn->setFont(font);

        horizontalLayout_3->addWidget(Back_Btn);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);


        verticalLayout->addWidget(widget_3);


        retranslateUi(Reader_Meau);

        QMetaObject::connectSlotsByName(Reader_Meau);
    } // setupUi

    void retranslateUi(QDialog *Reader_Meau)
    {
        Reader_Meau->setWindowTitle(QCoreApplication::translate("Reader_Meau", "\350\257\273\350\200\205\350\217\234\345\215\225", nullptr));
        Bor_Btn->setText(QCoreApplication::translate("Reader_Meau", "\345\200\237\344\271\246", nullptr));
        BackBook_Btn->setText(QCoreApplication::translate("Reader_Meau", "\350\277\230\344\271\246", nullptr));
        Back_Btn->setText(QCoreApplication::translate("Reader_Meau", "\350\277\224\345\233\236", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Reader_Meau: public Ui_Reader_Meau {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_READER_MEAU_H
