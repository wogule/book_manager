/********************************************************************************
** Form generated from reading UI file 'Books.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOOKS_H
#define UI_BOOKS_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include "bookcomp.h"

QT_BEGIN_NAMESPACE

class Ui_Books
{
public:
    QVBoxLayout *verticalLayout;
    BookComp *widget;
    QPushButton *Btn_Return;

    void setupUi(QDialog *Books)
    {
        if (Books->objectName().isEmpty())
            Books->setObjectName(QString::fromUtf8("Books"));
        Books->resize(1200, 840);
        Books->setMinimumSize(QSize(1200, 840));
        Books->setMaximumSize(QSize(1200, 840));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Books->setWindowIcon(icon);
        Books->setStyleSheet(QString::fromUtf8("QPushButton#Btn_Return{\n"
"	border-image: url(:/icon/back.png);\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"QPushButton#Btn_Return:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"QDialog{\n"
"	\n"
"	border-image: url(:/02.jpg);\n"
"}"));
        verticalLayout = new QVBoxLayout(Books);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        widget = new BookComp(Books);
        widget->setObjectName(QString::fromUtf8("widget"));
        Btn_Return = new QPushButton(widget);
        Btn_Return->setObjectName(QString::fromUtf8("Btn_Return"));
        Btn_Return->setGeometry(QRect(974, 57, 26, 26));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Btn_Return->sizePolicy().hasHeightForWidth());
        Btn_Return->setSizePolicy(sizePolicy);
        Btn_Return->setMinimumSize(QSize(26, 26));
        Btn_Return->setMaximumSize(QSize(26, 26));
        Btn_Return->setAutoDefault(false);

        verticalLayout->addWidget(widget);


        retranslateUi(Books);

        QMetaObject::connectSlotsByName(Books);
    } // setupUi

    void retranslateUi(QDialog *Books)
    {
        Books->setWindowTitle(QCoreApplication::translate("Books", "\345\233\276\344\271\246", nullptr));
        Btn_Return->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class Books: public Ui_Books {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOOKS_H
