/********************************************************************************
** Form generated from reading UI file 'Borrows.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BORROWS_H
#define UI_BORROWS_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Borrows
{
public:
    QHBoxLayout *horizontalLayout_2;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout;
    QTableView *Bor_View;
    QPushButton *Back_Btn;

    void setupUi(QDialog *Borrows)
    {
        if (Borrows->objectName().isEmpty())
            Borrows->setObjectName(QString::fromUtf8("Borrows"));
        Borrows->resize(785, 640);
        Borrows->setMinimumSize(QSize(785, 640));
        Borrows->setMaximumSize(QSize(785, 640));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Borrows->setWindowIcon(icon);
        Borrows->setStyleSheet(QString::fromUtf8("QDialog{\n"
"	\n"
"	border-image: url(:/08.png);\n"
"}\n"
"QPushButton{\n"
"	border-radius:5px;\n"
"	background-color:rgb(255, 254, 240,80);\n"
"}\n"
"\n"
"QPushButton:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}\n"
"\n"
"QTableView{\n"
"	background-color:rgb(255, 254, 240,70);\n"
"	border:2px solid rgb(255, 254, 240);\n"
"	border-radius:5px;\n"
"	selection-color: black;\n"
"    selection-background-color: rgb(255, 254, 240);\n"
"}\n"
""));
        horizontalLayout_2 = new QHBoxLayout(Borrows);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        widget_2 = new QWidget(Borrows);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        verticalLayout = new QVBoxLayout(widget_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        Bor_View = new QTableView(widget_2);
        Bor_View->setObjectName(QString::fromUtf8("Bor_View"));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font.setPointSize(16);
        Bor_View->setFont(font);

        verticalLayout->addWidget(Bor_View);

        Back_Btn = new QPushButton(widget_2);
        Back_Btn->setObjectName(QString::fromUtf8("Back_Btn"));
        Back_Btn->setFont(font);

        verticalLayout->addWidget(Back_Btn);


        horizontalLayout_2->addWidget(widget_2);


        retranslateUi(Borrows);

        QMetaObject::connectSlotsByName(Borrows);
    } // setupUi

    void retranslateUi(QDialog *Borrows)
    {
        Borrows->setWindowTitle(QCoreApplication::translate("Borrows", "\345\200\237\344\271\246", nullptr));
        Back_Btn->setText(QCoreApplication::translate("Borrows", "\350\277\224\345\233\236", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Borrows: public Ui_Borrows {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BORROWS_H
