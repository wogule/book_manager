/********************************************************************************
** Form generated from reading UI file 'Register.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REGISTER_H
#define UI_REGISTER_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Register
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *widget;
    QGridLayout *gridLayout;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QLineEdit *N_Led;
    QLineEdit *P_Led;
    QSpacerItem *verticalSpacer;
    QLineEdit *U_Led;
    QLabel *label_N;
    QLabel *label_U;
    QLabel *label_P;
    QSpacerItem *verticalSpacer_2;
    QWidget *widget_3;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_G;
    QRadioButton *M_RBtn;
    QSpacerItem *horizontalSpacer_7;
    QRadioButton *F_RBtn;
    QSpacerItem *horizontalSpacer_4;
    QWidget *widget_5;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label_A;
    QRadioButton *RBtn_OK;
    QSpacerItem *horizontalSpacer_8;
    QRadioButton *RBtn_NO;
    QSpacerItem *horizontalSpacer_6;
    QWidget *widget_2;
    QGridLayout *gridLayout_2;
    QLineEdit *Unit_Led;
    QSpacerItem *horizontalSpacer_9;
    QLabel *label_Unit;
    QSpacerItem *horizontalSpacer_10;
    QWidget *widget_4;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_11;
    QLabel *label_Number;
    QLineEdit *Num_Led;
    QSpacerItem *horizontalSpacer_12;
    QWidget *widget_6;
    QHBoxLayout *horizontalLayout_4;
    QSpacerItem *horizontalSpacer_13;
    QPushButton *Commit_Btn;
    QSpacerItem *horizontalSpacer_15;
    QPushButton *Back_Btn;
    QSpacerItem *horizontalSpacer_14;

    void setupUi(QWidget *Register)
    {
        if (Register->objectName().isEmpty())
            Register->setObjectName(QString::fromUtf8("Register"));
        Register->resize(800, 503);
        Register->setMinimumSize(QSize(800, 503));
        Register->setMaximumSize(QSize(800, 503));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Register->setWindowIcon(icon);
        Register->setStyleSheet(QString::fromUtf8("Register{\n"
"\n"
"	border-image: url(:/05.jpg);\n"
"}\n"
"QLabel{\n"
"	background-color: rgb(255, 254, 240,75);\n"
"	border-radius: 10px;\n"
"	color: rgb(0, 0, 0);\n"
"	font: bold 900 16px \"\345\215\216\346\226\207\346\245\267\344\275\223\";\n"
"}\n"
"QLineEdit{\n"
"	background-color: rgb(255, 254, 240);\n"
"	font-family: \"\345\215\216\346\226\207\346\245\267\344\275\223\"\n"
"}\n"
"QPushButton{\n"
"	background-color: rgb(255, 254, 240);\n"
"	font: bold 900 16px \"\345\215\216\346\226\207\346\245\267\344\275\223\";\n"
"}\n"
"QRadioButton{\n"
"	font: bold 900 16px \"\345\215\216\346\226\207\346\245\267\344\275\223\";\n"
"}"));
        verticalLayout = new QVBoxLayout(Register);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        widget = new QWidget(Register);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        gridLayout = new QGridLayout(widget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 3, 1, 1);

        N_Led = new QLineEdit(widget);
        N_Led->setObjectName(QString::fromUtf8("N_Led"));

        gridLayout->addWidget(N_Led, 4, 2, 1, 1);

        P_Led = new QLineEdit(widget);
        P_Led->setObjectName(QString::fromUtf8("P_Led"));
        P_Led->setEchoMode(QLineEdit::Password);

        gridLayout->addWidget(P_Led, 2, 2, 1, 1);

        verticalSpacer = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer, 1, 2, 1, 1);

        U_Led = new QLineEdit(widget);
        U_Led->setObjectName(QString::fromUtf8("U_Led"));

        gridLayout->addWidget(U_Led, 0, 2, 1, 1);

        label_N = new QLabel(widget);
        label_N->setObjectName(QString::fromUtf8("label_N"));

        gridLayout->addWidget(label_N, 4, 1, 1, 1);

        label_U = new QLabel(widget);
        label_U->setObjectName(QString::fromUtf8("label_U"));

        gridLayout->addWidget(label_U, 0, 1, 1, 1);

        label_P = new QLabel(widget);
        label_P->setObjectName(QString::fromUtf8("label_P"));

        gridLayout->addWidget(label_P, 2, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout->addItem(verticalSpacer_2, 3, 2, 1, 1);


        verticalLayout->addWidget(widget);

        widget_3 = new QWidget(Register);
        widget_3->setObjectName(QString::fromUtf8("widget_3"));
        sizePolicy.setHeightForWidth(widget_3->sizePolicy().hasHeightForWidth());
        widget_3->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(widget_3);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);

        label_G = new QLabel(widget_3);
        label_G->setObjectName(QString::fromUtf8("label_G"));

        horizontalLayout->addWidget(label_G);

        M_RBtn = new QRadioButton(widget_3);
        M_RBtn->setObjectName(QString::fromUtf8("M_RBtn"));
        M_RBtn->setChecked(true);

        horizontalLayout->addWidget(M_RBtn);

        horizontalSpacer_7 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_7);

        F_RBtn = new QRadioButton(widget_3);
        F_RBtn->setObjectName(QString::fromUtf8("F_RBtn"));

        horizontalLayout->addWidget(F_RBtn);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);


        verticalLayout->addWidget(widget_3);

        widget_5 = new QWidget(Register);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));
        sizePolicy.setHeightForWidth(widget_5->sizePolicy().hasHeightForWidth());
        widget_5->setSizePolicy(sizePolicy);
        horizontalLayout_3 = new QHBoxLayout(widget_5);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);

        label_A = new QLabel(widget_5);
        label_A->setObjectName(QString::fromUtf8("label_A"));

        horizontalLayout_3->addWidget(label_A);

        RBtn_OK = new QRadioButton(widget_5);
        RBtn_OK->setObjectName(QString::fromUtf8("RBtn_OK"));

        horizontalLayout_3->addWidget(RBtn_OK);

        horizontalSpacer_8 = new QSpacerItem(20, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_8);

        RBtn_NO = new QRadioButton(widget_5);
        RBtn_NO->setObjectName(QString::fromUtf8("RBtn_NO"));
        RBtn_NO->setChecked(true);

        horizontalLayout_3->addWidget(RBtn_NO);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_6);


        verticalLayout->addWidget(widget_5);

        widget_2 = new QWidget(Register);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        sizePolicy.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
        widget_2->setSizePolicy(sizePolicy);
        gridLayout_2 = new QGridLayout(widget_2);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        Unit_Led = new QLineEdit(widget_2);
        Unit_Led->setObjectName(QString::fromUtf8("Unit_Led"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(Unit_Led->sizePolicy().hasHeightForWidth());
        Unit_Led->setSizePolicy(sizePolicy1);

        gridLayout_2->addWidget(Unit_Led, 0, 2, 1, 2);

        horizontalSpacer_9 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_9, 0, 0, 1, 1);

        label_Unit = new QLabel(widget_2);
        label_Unit->setObjectName(QString::fromUtf8("label_Unit"));

        gridLayout_2->addWidget(label_Unit, 0, 1, 1, 1);

        horizontalSpacer_10 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_10, 0, 4, 1, 1);


        verticalLayout->addWidget(widget_2);

        widget_4 = new QWidget(Register);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        sizePolicy.setHeightForWidth(widget_4->sizePolicy().hasHeightForWidth());
        widget_4->setSizePolicy(sizePolicy);
        horizontalLayout_2 = new QHBoxLayout(widget_4);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_11 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_11);

        label_Number = new QLabel(widget_4);
        label_Number->setObjectName(QString::fromUtf8("label_Number"));

        horizontalLayout_2->addWidget(label_Number);

        Num_Led = new QLineEdit(widget_4);
        Num_Led->setObjectName(QString::fromUtf8("Num_Led"));

        horizontalLayout_2->addWidget(Num_Led);

        horizontalSpacer_12 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_12);


        verticalLayout->addWidget(widget_4);

        widget_6 = new QWidget(Register);
        widget_6->setObjectName(QString::fromUtf8("widget_6"));
        sizePolicy.setHeightForWidth(widget_6->sizePolicy().hasHeightForWidth());
        widget_6->setSizePolicy(sizePolicy);
        horizontalLayout_4 = new QHBoxLayout(widget_6);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSpacer_13 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_13);

        Commit_Btn = new QPushButton(widget_6);
        Commit_Btn->setObjectName(QString::fromUtf8("Commit_Btn"));

        horizontalLayout_4->addWidget(Commit_Btn);

        horizontalSpacer_15 = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_15);

        Back_Btn = new QPushButton(widget_6);
        Back_Btn->setObjectName(QString::fromUtf8("Back_Btn"));

        horizontalLayout_4->addWidget(Back_Btn);

        horizontalSpacer_14 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_14);


        verticalLayout->addWidget(widget_6);

        QWidget::setTabOrder(U_Led, P_Led);
        QWidget::setTabOrder(P_Led, N_Led);
        QWidget::setTabOrder(N_Led, M_RBtn);
        QWidget::setTabOrder(M_RBtn, F_RBtn);
        QWidget::setTabOrder(F_RBtn, RBtn_OK);
        QWidget::setTabOrder(RBtn_OK, RBtn_NO);
        QWidget::setTabOrder(RBtn_NO, Unit_Led);
        QWidget::setTabOrder(Unit_Led, Num_Led);
        QWidget::setTabOrder(Num_Led, Commit_Btn);
        QWidget::setTabOrder(Commit_Btn, Back_Btn);

        retranslateUi(Register);

        QMetaObject::connectSlotsByName(Register);
    } // setupUi

    void retranslateUi(QWidget *Register)
    {
        Register->setWindowTitle(QCoreApplication::translate("Register", "\347\224\250\346\210\267\346\263\250\345\206\214", nullptr));
        N_Led->setPlaceholderText(QCoreApplication::translate("Register", "\350\276\223\345\205\245\345\247\223\345\220\215", nullptr));
        P_Led->setPlaceholderText(QCoreApplication::translate("Register", "\350\256\276\347\275\256\345\257\206\347\240\201", nullptr));
        U_Led->setPlaceholderText(QCoreApplication::translate("Register", "\350\276\223\345\205\245\345\233\276\344\271\246\350\257\201\345\217\267", nullptr));
        label_N->setText(QCoreApplication::translate("Register", "\345\247\223\345\220\215\357\274\232", nullptr));
        label_U->setText(QCoreApplication::translate("Register", "\350\264\246\345\217\267\357\274\232", nullptr));
        label_P->setText(QCoreApplication::translate("Register", "\345\257\206\347\240\201\357\274\232", nullptr));
        label_G->setText(QCoreApplication::translate("Register", "\346\200\247\345\210\253\357\274\232", nullptr));
        M_RBtn->setText(QCoreApplication::translate("Register", "\347\224\267", nullptr));
        F_RBtn->setText(QCoreApplication::translate("Register", "\345\245\263", nullptr));
        label_A->setText(QCoreApplication::translate("Register", "\347\256\241\347\220\206\345\221\230", nullptr));
        RBtn_OK->setText(QCoreApplication::translate("Register", "\346\230\257", nullptr));
        RBtn_NO->setText(QCoreApplication::translate("Register", "\345\220\246", nullptr));
        Unit_Led->setPlaceholderText(QCoreApplication::translate("Register", "\350\276\223\345\205\245\345\267\245\344\275\234\345\234\260", nullptr));
        label_Unit->setText(QCoreApplication::translate("Register", "\345\215\225  \344\275\215:", nullptr));
        label_Number->setText(QCoreApplication::translate("Register", "\346\211\213\346\234\272\345\217\267\357\274\232", nullptr));
        Num_Led->setPlaceholderText(QCoreApplication::translate("Register", "\350\276\223\345\205\245\346\211\213\346\234\272\345\217\267", nullptr));
        Commit_Btn->setText(QCoreApplication::translate("Register", "\346\217\220\344\272\244", nullptr));
        Back_Btn->setText(QCoreApplication::translate("Register", "\350\277\224\345\233\236", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Register: public Ui_Register {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REGISTER_H
