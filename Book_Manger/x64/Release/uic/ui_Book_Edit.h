/********************************************************************************
** Form generated from reading UI file 'Book_Edit.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BOOK_EDIT_H
#define UI_BOOK_EDIT_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "bookedit_comp.h"

QT_BEGIN_NAMESPACE

class Ui_Book_Edit
{
public:
    QVBoxLayout *verticalLayout;
    BookEdit_Comp *widget;
    QWidget *widget_2;
    QHBoxLayout *horizontalLayout;
    QPushButton *Btn_OK;

    void setupUi(QDialog *Book_Edit)
    {
        if (Book_Edit->objectName().isEmpty())
            Book_Edit->setObjectName(QString::fromUtf8("Book_Edit"));
        Book_Edit->resize(433, 351);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/icon.ico"), QSize(), QIcon::Normal, QIcon::Off);
        Book_Edit->setWindowIcon(icon);
        Book_Edit->setStyleSheet(QString::fromUtf8("QDialog{\n"
"	border-image: url(:/04.jpg);\n"
"}\n"
"QPushButton{\n"
"	background-color:rgb(255, 254, 240,80);\n"
"	border-radius:5px;\n"
"	\n"
"}\n"
"QPushButton:hover{\n"
"	border:2px solid rgb(255, 254, 240);\n"
"}"));
        verticalLayout = new QVBoxLayout(Book_Edit);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        widget = new BookEdit_Comp(Book_Edit);
        widget->setObjectName(QString::fromUtf8("widget"));

        verticalLayout->addWidget(widget);

        widget_2 = new QWidget(Book_Edit);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
        widget_2->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(widget_2);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Btn_OK = new QPushButton(widget_2);
        Btn_OK->setObjectName(QString::fromUtf8("Btn_OK"));
        QFont font;
        font.setFamily(QString::fromUtf8("\345\215\216\346\226\207\346\245\267\344\275\223"));
        font.setPointSize(16);
        Btn_OK->setFont(font);

        horizontalLayout->addWidget(Btn_OK);


        verticalLayout->addWidget(widget_2);


        retranslateUi(Book_Edit);

        QMetaObject::connectSlotsByName(Book_Edit);
    } // setupUi

    void retranslateUi(QDialog *Book_Edit)
    {
        Book_Edit->setWindowTitle(QCoreApplication::translate("Book_Edit", "Book_Edit", nullptr));
        Btn_OK->setText(QCoreApplication::translate("Book_Edit", "\347\241\256\350\256\244", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Book_Edit: public Ui_Book_Edit {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BOOK_EDIT_H
