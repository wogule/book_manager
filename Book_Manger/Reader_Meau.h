#pragma once

#include <QDialog>
#include "ui_Reader_Meau.h"
#include "Borrow_Book.h"
#include "Back_Book.h"

class Reader_Meau : public QDialog
{
	Q_OBJECT

public:
	Reader_Meau(QWidget *parent = Q_NULLPTR);
	~Reader_Meau();
public slots:
	void BackMain();
signals:
	void Back_Main();
private:
	Ui::Reader_Meau ui;
	Borrow_Book Borbook;
	Back_Book Bacbook;
	
};
