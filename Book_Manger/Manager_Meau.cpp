#include "Manager_Meau.h"
Manager_Meau::Manager_Meau(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
	setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
	//跳转登录
	connect(ui.Back_Btn, &QPushButton::clicked, this, &Manager_Meau::ReturnMain);
	//跳转图书
	connect(ui.Book_Btn, &QPushButton::clicked, this, &Manager_Meau::Book);
	//跳转借阅
	connect(ui.Borrow_Btn, &QPushButton::clicked, this, &Manager_Meau::Borrow);
	//跳转分类
	connect(ui.Classfy_Btn, &QPushButton::clicked, this, &Manager_Meau::Classfy);
	//跳转罚款
	connect(ui.Fine_Btn, &QPushButton::clicked, this, &Manager_Meau::Fine);
	//跳转用户
	connect(ui.Read_Btn, &QPushButton::clicked, this, &Manager_Meau::Users);
	//////////////////////////////////////从子窗口返回
	
	//从图书返回菜单
	connect(&book, &Books::BackBook, [=](){book.hide();this->show();});
	//从分类返回菜单
	connect(&Class, &Classification::BackClassfy, [=]() {Class.hide(); this->show(); });
	//从借阅返回菜单
	connect(&borr, &Borrows::BackBorrow, [=](){borr.hide();this->show();});
	//从罚款返回菜单
	connect(&fine, &Forfeit::BackFine, [=](){fine.hide();this->show();});
	//从用户返回菜单
	connect(&users, &User::BackUsers, [=](){users.hide();this->show();});
	
}

Manager_Meau::~Manager_Meau()
{
}
void Manager_Meau::Classfy()
{
	this->hide();
	Class.SqlQuery();
	Class.show();
}
void Manager_Meau::Fine()
{
	this->hide();
	fine.SqlQuery();
	fine.show();
}
void Manager_Meau::Borrow()
{
	this->hide();
	borr.SqlQuery();
	borr.show();
}
void Manager_Meau::Book()
{
	this->hide();
	book.ui.widget->SqlQuery();
	book.show();
	
}
void Manager_Meau::Users()
{
	this->hide();
	users.SqlQuery();
	users.show();
}
void Manager_Meau::ReturnMain() {
	emit Back_Main();
}