#pragma once

#include <QDialog>
#include "ui_Books.h"

class Books : public QDialog
{
	Q_OBJECT

public:
	Books(QWidget *parent = Q_NULLPTR);
	~Books();
signals:
	void BackBook();
public:
	Ui::Books ui;
};
