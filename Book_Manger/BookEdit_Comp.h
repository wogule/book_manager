#pragma once

#include <QWidget>
#include "ui_BookEdit_Comp.h"
#include "DBHelper.h"
class BookEdit_Comp : public QWidget
{
	Q_OBJECT

public:
	BookEdit_Comp(QWidget *parent = Q_NULLPTR);
	~BookEdit_Comp();
	bool Book_Show(QString );
	bool Book_Add();
	void Class_Add();//将类型读取到combox
	bool Book_Update();
public slots:
	

private:
	Ui::BookEdit_Comp ui;
	
};
