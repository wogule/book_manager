#include "DBHelper.h"
QSqlDatabase DBHelper::db = QSqlDatabase::addDatabase("QODBC");//数据库驱动类型为SQL Server
bool DBHelper::OpenDatabase()
{
    QString dsn = QString::fromLocal8Bit("design");      //数据源名称
    db.setHostName("localhost");                        //选择本地主机，127.0.1.1
    db.setDatabaseName(dsn);                            //设置数据源名称
    db.setUserName("sa");                               //登录用户
    db.setPassword("root");                              //密码
    if (!db.open())                                      //打开数据库
    {
        QMessageBox::critical(0, QObject::tr("Database error"), db.lastError().text());
        return false;                                   //打开失败
    }
    return true;
}
bool DBHelper::CloseDatabase() {
    if (db.isOpen())
    {
        db.close();
        return true;
    }
    else
        return false;
}
bool DBHelper::QueryDatabase(char* SqlContent)
{
    OpenDatabase();
    QSqlQuery query(db); //查询design表并输出，测试能否正常操作数据库
    //qDebug() << QString::fromLocal8Bit(SqlContent);
    try {
        if (!query.exec(QString::fromLocal8Bit(SqlContent))) {
            CloseDatabase();
            return false;
        }
    }
    catch (exception e) {

    }
   //qDebug() << "query data success!";
    while (query.next())
    {
        //qDebug() << query.value(0) << query.value(1) << query.value(2);
        if (QString::fromLocal8Bit(SqlContent).contains(query.value(0).toString()))
        {
            CloseDatabase();
            return true;
        }
    }
       
    return false;
    
}
bool DBHelper::ADDData(char* SqlContent) {
    OpenDatabase();
    QSqlQuery query(db);
    //QString a = QStringLiteral("INSERT INTO Users VALUES('182056202', '0001', '我m', '男', 20, '山西省', '12345685121', '借阅者')");
    qDebug() << QString::fromLocal8Bit(SqlContent);
    try {
        if (!query.exec(QString::fromLocal8Bit(SqlContent))) {
            CloseDatabase();
            return false;
        }
    }
    catch(exception e){
        
    }
    
    
    //qDebug() << "insert data success!";
    CloseDatabase();
    return true;
   
}
bool DBHelper::DeleteData(char* SqlContent)
{
    OpenDatabase();
    QSqlQuery query(db);
    try {
        if (!query.exec(QString::fromLocal8Bit(SqlContent))) {
            CloseDatabase();
            return false;
        }
    }
    catch (exception e) {

    }
    //qDebug() << "DELETE data success!";
    CloseDatabase();
    return true;

}
bool DBHelper::UpdateData(char* SqlContent)
{
    OpenDatabase();
    QSqlQuery query(db);
    qDebug() << QString::fromLocal8Bit(SqlContent);
    try {
        if (!query.exec(QString::fromLocal8Bit(SqlContent))) {
            CloseDatabase();
            return false;
        }
    }
    catch (exception e) {

    }
   //qDebug() << "UPDATE data success!";
   CloseDatabase();
   return true;

}