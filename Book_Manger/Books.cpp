#include "Books.h"

Books::Books(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
    setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
    connect(ui.Btn_Return, &QPushButton::clicked, [=]() {emit BackBook(); });
    
}

Books::~Books()
{
}
