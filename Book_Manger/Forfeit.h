#pragma once

#include <QDialog>
#include "ui_Forfeit.h"
#include "DBHelper.h"
class Forfeit : public QDialog
{
	Q_OBJECT

public:
	Forfeit(QWidget *parent = Q_NULLPTR);
	~Forfeit();
	bool SqlQuery();
public slots:
	void Return_Main();
signals:
	void BackFine();
private:
	Ui::Forfeit ui;
};
