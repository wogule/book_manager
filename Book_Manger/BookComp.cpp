#include "BookComp.h"
#include <QDebug>

BookComp::BookComp(QWidget *parent)
	: QWidget(parent)
{
    
	ui.setupUi(this);
    connect(ui.S_led, &QLineEdit::textChanged, [=]() {Check(); });
    connect(ui.All_Btn, &QPushButton::clicked, [=]() {SqlQuery(); });
    connect(ui.View, &QTableView::clicked, [=]() {
        int row = ui.View->currentIndex().row();
        QAbstractItemModel* model = ui.View->model();
        QModelIndex index = model->index(row, 0);//选中行第一列的内容
        QString Book_Name = index.data().toString();
        ui.B_Led->setText(Book_Name);
        });
    connect(ui.Delete_Btn, &QPushButton::clicked, this, &BookComp::Delete);
    connect(ui.Delete_Btn, &QPushButton::clicked, [=]() {SqlQuery(); ui.B_Led->setText(""); });//删除并实时更新
    connect(ui.Update_Btn, &QPushButton::clicked, this, &BookComp::Update);
    connect(ui.Add_Btn, &QPushButton::clicked, this, &BookComp::Add);
    connect(Be.ui.Btn_OK, &QPushButton::clicked, this, &BookComp::Select);
    
}

BookComp::~BookComp()
{ 
}
bool BookComp::SqlQuery(char* SqlContent)
{
    if (DBHelper::OpenDatabase()) {
       
        QSqlQueryModel* model = new QSqlQueryModel(this);
        //QSqlTableModel* model = new QSqlTableModel(this,DBHelper::db);
        //model->setTable("Book")
        //"SELECT B_name,B_writer,B_publish,B_price,B_Class,Num_total,Num_remain FROM Book,Classfy WHERE Book.Class_no=Classfy.Class_no"
         //model->setSort(1, Qt::AscendingOrder);
         //model->select();
         //        view->setColumnHidden(0, true);
        //view->resizeColumnsToContents();
        //view->setRowHidden(0, true);
        //view->setColumnHidden(0, true);
        model->setQuery(QString::fromLocal8Bit(SqlContent));
        model->setHeaderData(0, Qt::Horizontal, QStringLiteral("书名"));
        model->setHeaderData(1, Qt::Horizontal, QStringLiteral("作者"));
        model->setHeaderData(2, Qt::Horizontal, QStringLiteral("出版"));
        model->setHeaderData(3, Qt::Horizontal, QStringLiteral("价格"));
        model->setHeaderData(4, Qt::Horizontal, QStringLiteral("分类"));
        model->setHeaderData(5, Qt::Horizontal, QStringLiteral("总数"));
        model->setHeaderData(6, Qt::Horizontal, QStringLiteral("剩余"));
        
        ui.View->setModel(model);
        ui.View->setSelectionMode(QAbstractItemView::SingleSelection);//不能连续选
        ui.View->setSelectionBehavior(QAbstractItemView::SelectRows);//按行选
        ui.View->setEditTriggers(QAbstractItemView::NoEditTriggers);//不能在 表格上编辑
        ui.View->horizontalHeader()->setStyleSheet("QHeaderView::section {"
            "background-color:rgb(255, 254, 240,70);}");
        
        ui.View->verticalHeader()->hide();
        QHeaderView* header = ui.View->horizontalHeader();
        header->setStretchLastSection(false);//拉伸最后一段
        
        
        ui.View->setFixedWidth(ui.View->columnWidth(0) +
            ui.View->columnWidth(1) +
            ui.View->columnWidth(2) +
            ui.View->columnWidth(3) +
            ui.View->columnWidth(4) +
            ui.View->columnWidth(5) +
            ui.View->columnWidth(6)+5);
        
        return true;
       
    }
    return false;
}
bool BookComp::Check()
{
    QString SqlContent;
    if(ui.Classes_comb->currentIndex()==0)//选择按书名查找
        SqlContent = QString("SELECT B_name,B_writer,B_publish,B_price,B_Class,Num_total,Num_remain FROM Book,Classfy WHERE Book.Class_no=Classfy.Class_no AND B_name like'%%1%'").arg(ui.S_led->text());
    else//选择按类别查找
        SqlContent = QString("SELECT B_name,B_writer,B_publish,B_price,B_Class,Num_total,Num_remain FROM Book,Classfy WHERE Book.Class_no=Classfy.Class_no AND B_Class like'%%1%'").arg(ui.S_led->text());
    QByteArray byte;  byte = SqlContent.toLocal8Bit();
    char* ch;  ch = byte.data();
    if (SqlQuery(ch))
        return true;
    return false;
}

bool BookComp::Add()//添加书
{
    /*
    * 弹出添加书的界面
    */
    
    flag = true;
    Be.ui.widget->Class_Add();
    Be.setWindowModality(Qt::ApplicationModal);//父窗口不能操作
    Be.show();
    SqlQuery();
    return false;
}

bool BookComp::Update()//更新书
{
    /*
    * 弹出更新书的界面
    */
    
    flag = false;
    Be.ui.widget->Book_Show(ui.B_Led->text());
    Be.setWindowModality(Qt::ApplicationModal);//父窗口不能操作
    Be.show();
    ui.B_Led->setText("");
    SqlQuery();
    return false;
}

bool BookComp::Delete()//删除书
{
    if (ui.B_Led->text().isEmpty())
        return false;
    else {
        QString SqlContent = QString("DELETE FROM Book WHERE B_name='%1'").arg(ui.B_Led->text());
        QByteArray byte;  byte = SqlContent.toLocal8Bit();
        char* ch;  ch = byte.data();
        if (DBHelper::DeleteData(ch))
            return true;
    }
    return false;
}
void BookComp::Select() {
    if (!flag)
    {
        Be.ui.widget->Book_Update();
    }
    else {
        Be.ui.widget->Book_Add();
    }
    Be.close(); 
    SqlQuery();
}