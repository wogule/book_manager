#pragma once

#include <QDialog>
#include "ui_Manager_Meau.h"
#include "Books.h"
#include "Classification.h"
#include "Borrows.h"
#include "Forfeit.h"
#include "User.h"

class Manager_Meau : public QDialog
{
	Q_OBJECT

public:
	Manager_Meau(QWidget *parent = Q_NULLPTR);
	~Manager_Meau();
public slots:
	void ReturnMain();//返回登录界面
	void Classfy();//跳转到类别界面
	void Fine();//跳转到罚款界面
	void Borrow();//跳转到借阅界面
	void Book();//跳转到图书界面
	void Users();//跳转到用户信息界面
signals:
	void Back_Main();
private:
	Ui::Manager_Meau ui;
	Books book;
	Classification Class;
	Borrows borr;
	Forfeit fine;
	User users;
	
};
